﻿using System.Data.SqlClient;
using EtlDataBase.Context;
using Microsoft.EntityFrameworkCore;

namespace EtlDataBase.Repository
{
    public interface IRepository<T> where T : class
    {
        Task<List<T>> GetAllAsync();
        Task<List<Dictionary<string, string>>> SelectFromTableAsync(string tableName, List<string>? columns, CancellationToken token);
    }

    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly EtlDbContext context;
        private readonly IConfiguration config;

        public Repository(EtlDbContext context, IConfiguration config)
        {
            this.context = context;
            this.config = config;

        }
        public async Task<List<T>> GetAllAsync()
        {
            return await context.Set<T>().ToListAsync();
        }

        public async Task<List<Dictionary<string, string>>> SelectFromTableAsync(string tableName, List<string>? columns, CancellationToken token)
        {
            var connectionString = config.GetConnectionString("MySqlConnection");
            string strColums = columns is not null? string.Join(",", columns) : "*";
            var query = $"SELECT {strColums} FROM {tableName}";
            var res = new List<Dictionary<string, string>>();

            using (SqlConnection connection = new(connectionString))
            {
                await connection.OpenAsync();

                using (SqlCommand command = new(query,connection))
                {
                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var result = new Dictionary<string, string>();

                            for (int i = 0; i < columns.Count; i++)
                            {
                                result[reader.GetName(i).ToString()] = reader[i].ToString();
                            }

                            res.Add(result);
                        }
                    }
                }
            }

            return res;
        }
    }
}
