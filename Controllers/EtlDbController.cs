using EtlDataBase.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace EtlDataBase.Controller
{
    [Route("api/[controller]")]
    [ApiController]

    public class EtlDbController : ControllerBase
    {
        private readonly IRepository<Prospect> prospect;

        public EtlDbController(IRepository<Prospect> prospect)
        {
            this.prospect = prospect;
        }

        [HttpGet("test")]
        public async Task<IActionResult> GetAllProspects()
        {
            try
            {
                var data = await prospect.GetAllAsync();

                return Ok(data);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }



        [HttpGet("test-connection")]
        public async Task<IActionResult> TestConnection()
        {
            try
            {
                var connectionString = "Server=172.19.8.18,1700;Database=gateway_db;User Id=sa;Password=vnp-1234";



                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();

                    if (connection.State == ConnectionState.Open)
                    {
                        Console.WriteLine("Connection is successful");
                    }

                    return Ok(connectionString);

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }


        [HttpPost("run-script")]
        public async Task<IActionResult> RunSqlScript(string tableName, List<string>? columns = null, CancellationToken token = default)
        {
            try
            {
                var data = await prospect.SelectFromTableAsync(tableName, columns, token);
                return Ok(data);
            }
            catch (Exception ex)
            {
                throw new Exception("There was an error in retrieving information: " + ex.Message);
            }
        }
    }
}
