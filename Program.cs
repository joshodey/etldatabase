using EtlDataBase;
using EtlDataBase.Context;
using EtlDataBase.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<EtlDbContext>(option =>
option.UseSqlServer(builder.Configuration.GetConnectionString("MySqlConnection"), options => options.EnableRetryOnFailure(
    maxRetryCount: 3,
    maxRetryDelay: System.TimeSpan.FromSeconds(30),
    errorNumbersToAdd: null)));
builder.Services.AddScoped<IRepository<Prospect>, Repository<Prospect>>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
