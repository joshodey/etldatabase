﻿using Microsoft.EntityFrameworkCore;

namespace EtlDataBase.Context
{
    public class EtlDbContext : DbContext
    {
        public EtlDbContext(DbContextOptions<EtlDbContext> options) : base(options)
        {}

        public DbSet<Prospect> prospect { get; set; }
    }
}
