﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Processor
{
    public int Id { get; set; }

    public string Code { get; set; } = null!;

    public string Name { get; set; } = null!;

    public bool IsEnabled { get; set; }
}
