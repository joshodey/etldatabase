﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Currency
{
    public int CurrencyId { get; set; }

    public string Number { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public bool IsActive { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public string? Symbol { get; set; }
}
