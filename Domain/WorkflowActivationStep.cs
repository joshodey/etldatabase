﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class WorkflowActivationStep
{
    public int WorkflowActivationStepId { get; set; }

    public string? Name { get; set; }

    public string? Description { get; set; }

    public int RoleId { get; set; }

    public bool IsActive { get; set; }

    public int Rank { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual Role Role { get; set; } = null!;

    public virtual ICollection<WorkflowActivationLog> WorkflowActivationLogs { get; set; } = new List<WorkflowActivationLog>();

    public virtual ICollection<WorkflowActivationStepDetail> WorkflowActivationStepDetails { get; set; } = new List<WorkflowActivationStepDetail>();

    public virtual ICollection<WorkflowActivation> WorkflowActivations { get; set; } = new List<WorkflowActivation>();
}
