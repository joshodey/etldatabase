﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class CreateOrder
{
    public int CreateOrderId { get; set; }

    public string? TransactionId { get; set; }

    public int PartyId { get; set; }

    public string? Lastname { get; set; }

    public string? Firstname { get; set; }

    public string? EmailAddress { get; set; }

    public string? PhoneNumber { get; set; }

    public string? Description { get; set; }

    public string? PaymentType { get; set; }

    public decimal Amount { get; set; }

    public int Currency { get; set; }

    public string? CustomFields { get; set; }

    public string? CustomParam { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public string? Status { get; set; }

    public string? Approved { get; set; }
}
