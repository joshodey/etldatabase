﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class PaymentSubscription
{
    public int PaymentSubscriptionId { get; set; }

    public string? CustomerName { get; set; }

    public string CustomerEmail { get; set; } = null!;

    public string SendNotificationToEmail { get; set; } = null!;

    public decimal Amount { get; set; }

    public bool IsActive { get; set; }

    public string PaymentInterval { get; set; } = null!;

    public int PaymentFrequency { get; set; }

    public int FrequencyCount { get; set; }

    public string? ExtraField { get; set; }

    public DateTime DateSubscribed { get; set; }

    public DateTime DateModified { get; set; }

    public DateTime LastChargedDate { get; set; }

    public DateTime NextChargeDate { get; set; }

    public int PartyId { get; set; }

    public virtual Party Party { get; set; } = null!;
}
