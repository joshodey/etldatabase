﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class WorkflowActivation
{
    public int WorkflowActivationId { get; set; }

    public string? Status { get; set; }

    public int PartyId { get; set; }

    public int UserId { get; set; }

    public int WorkflowActivationStepId { get; set; }

    public bool IsMerchant { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual Party Party { get; set; } = null!;

    public virtual ICollection<WorkflowActivationDetail> WorkflowActivationDetails { get; set; } = new List<WorkflowActivationDetail>();

    public virtual WorkflowActivationStep WorkflowActivationStep { get; set; } = null!;
}
