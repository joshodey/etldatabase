﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class WorkflowActivationDetail
{
    public int WorkflowActivationDetailId { get; set; }

    public bool IsApproved { get; set; }

    public string Reason { get; set; } = null!;

    public string? Comment { get; set; }

    public int WorkflowActivationId { get; set; }

    public int UserId { get; set; }

    public int WorkflowActivationStepId { get; set; }

    public string UserName { get; set; } = null!;

    public bool IsMerchant { get; set; }

    public int PartyId { get; set; }

    public string? RequirementDetail { get; set; }

    public string StaffRole { get; set; } = null!;

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual User User { get; set; } = null!;

    public virtual WorkflowActivation WorkflowActivation { get; set; } = null!;
}
