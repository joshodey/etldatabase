﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Syspublication
{
    public string? Description { get; set; }

    public string Name { get; set; } = null!;

    public int Pubid { get; set; }

    public byte ReplFreq { get; set; }

    public byte Status { get; set; }

    public byte SyncMethod { get; set; }

    public byte[]? SnapshotJobid { get; set; }

    public bool IndependentAgent { get; set; }

    public bool ImmediateSync { get; set; }

    public bool EnabledForInternet { get; set; }

    public bool AllowPush { get; set; }

    public bool AllowPull { get; set; }

    public bool AllowAnonymous { get; set; }

    public bool ImmediateSyncReady { get; set; }

    public bool AllowSyncTran { get; set; }

    public bool AutogenSyncProcs { get; set; }

    public int? Retention { get; set; }

    public bool AllowQueuedTran { get; set; }

    public bool? SnapshotInDefaultfolder { get; set; }

    public string? AltSnapshotFolder { get; set; }

    public string? PreSnapshotScript { get; set; }

    public string? PostSnapshotScript { get; set; }

    public bool CompressSnapshot { get; set; }

    public string FtpAddress { get; set; } = null!;

    public int FtpPort { get; set; }

    public string? FtpSubdirectory { get; set; }

    public string FtpLogin { get; set; } = null!;

    public string? FtpPassword { get; set; }

    public bool AllowDts { get; set; }

    public bool AllowSubscriptionCopy { get; set; }

    public bool? CentralizedConflicts { get; set; }

    public int? ConflictRetention { get; set; }

    public int? ConflictPolicy { get; set; }

    public int? QueueType { get; set; }

    public string AdGuidname { get; set; } = null!;

    public int BackwardCompLevel { get; set; }

    public bool AllowInitializeFromBackup { get; set; }

    public byte[]? MinAutonosyncLsn { get; set; }

    public int? ReplicateDdl { get; set; }

    public int Options { get; set; }

    public int? OriginatorId { get; set; }
}
