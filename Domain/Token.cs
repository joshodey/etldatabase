﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Token
{
    public int TokenId { get; set; }

    public int UserId { get; set; }

    public string? AccessToken { get; set; }

    public DateTime AtexpiryTime { get; set; }

    public string? RefreshToken { get; set; }

    public DateTime RtexpiryTime { get; set; }

    public string? LoginLocation { get; set; }

    public string? Ipaddress { get; set; }

    public DateTime LoginTime { get; set; }

    public bool IsExpired { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }
}
