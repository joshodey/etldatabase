﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MspeerTopologyrequest
{
    public int Id { get; set; }

    public string Publication { get; set; } = null!;

    public DateTime? SentDate { get; set; }
}
