﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MessageTemplate
{
    public int MessageId { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public string Message1 { get; set; } = null!;

    public string Message2 { get; set; } = null!;

    public string DashboardMessage { get; set; } = null!;

    public string? Subject { get; set; }

    public string? ClientCopy { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual ICollection<Notification> Notifications { get; set; } = new List<Notification>();
}
