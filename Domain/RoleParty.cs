﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class RoleParty
{
    public int RolePartyId { get; set; }

    public int RoleId { get; set; }

    public int PartyId { get; set; }

    public int UserId { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual Party Party { get; set; } = null!;

    public virtual Role Role { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}
