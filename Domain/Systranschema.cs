﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Systranschema
{
    public int Tabid { get; set; }

    public byte[] Startlsn { get; set; } = null!;

    public byte[] Endlsn { get; set; } = null!;

    public int Typeid { get; set; }
}
