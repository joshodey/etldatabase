﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Party
{
    public int Id { get; set; }

    public string Code { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string? Bank { get; set; }

    public string? Account { get; set; }

    public string Email { get; set; } = null!;

    public bool IsEnabled { get; set; }

    public bool IsMerchant { get; set; }

    public string? Url { get; set; }

    public int? ProfileId { get; set; }

    public bool? FeeIncluded { get; set; }

    public string? SecretKey { get; set; }

    public string? Ipaddresses { get; set; }

    public string? MerchantId { get; set; }

    public string? MerId { get; set; }

    public string? ProfileName { get; set; }

    public string? TerminalId { get; set; }

    public string? Discriminator { get; set; }

    public string? RetailerId { get; set; }

    public bool Migrated { get; set; }

    public string? AmexId { get; set; }

    public bool? IsCardEnabled { get; set; }

    public bool? IsRecurring { get; set; }

    public string? PayattitudeFee { get; set; }

    public string? CardFee { get; set; }

    public decimal? MaxFee { get; set; }

    public decimal? Vat { get; set; }

    public bool? AddVat { get; set; }

    public string? EncryptionKey { get; set; }

    public string? EncryptionIv { get; set; }

    public bool? ReversalEnabled { get; set; }

    public bool? EnableCardOnFile { get; set; }

    public string? CardOnFileRef { get; set; }

    public string? CardOnFileId { get; set; }

    public int? OrderTimeOut { get; set; }

    public bool? DisablePayattitude { get; set; }

    public virtual ICollection<BankAccount> BankAccounts { get; set; } = new List<BankAccount>();

    public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();

    public virtual ICollection<PaymentPage> PaymentPages { get; set; } = new List<PaymentPage>();

    public virtual ICollection<PaymentPlan> PaymentPlans { get; set; } = new List<PaymentPlan>();

    public virtual ICollection<PaymentSubscription> PaymentSubscriptions { get; set; } = new List<PaymentSubscription>();

    public virtual Preference? Preference { get; set; }

    public virtual Profile? Profile { get; set; }

    public virtual ICollection<RoleParty> RoleParties { get; set; } = new List<RoleParty>();

    public virtual ICollection<RolePermission> RolePermissions { get; set; } = new List<RolePermission>();

    public virtual ICollection<SettlementItem> SettlementItems { get; set; } = new List<SettlementItem>();

    public virtual ICollection<SettlementRule> SettlementRules { get; set; } = new List<SettlementRule>();

    public virtual ICollection<Transaction> Transactions { get; set; } = new List<Transaction>();

    public virtual ICollection<Upload> Uploads { get; set; } = new List<Upload>();

    public virtual ICollection<UserParty> UserParties { get; set; } = new List<UserParty>();

    public virtual ICollection<UserRole> UserRoles { get; set; } = new List<UserRole>();

    public virtual ICollection<WorkflowActivationLog> WorkflowActivationLogs { get; set; } = new List<WorkflowActivationLog>();

    public virtual ICollection<WorkflowActivation> WorkflowActivations { get; set; } = new List<WorkflowActivation>();
}
