﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Sysarticle
{
    public int Artid { get; set; }

    public string? CreationScript { get; set; }

    public string? DelCmd { get; set; }

    public string? Description { get; set; }

    public string DestTable { get; set; } = null!;

    public int Filter { get; set; }

    public string? FilterClause { get; set; }

    public string? InsCmd { get; set; }

    public string Name { get; set; } = null!;

    public int Objid { get; set; }

    public int Pubid { get; set; }

    public byte PreCreationCmd { get; set; }

    public byte Status { get; set; }

    public int SyncObjid { get; set; }

    public byte Type { get; set; }

    public string? UpdCmd { get; set; }

    public byte[]? SchemaOption { get; set; }

    public string DestOwner { get; set; } = null!;

    public int? InsScriptingProc { get; set; }

    public int? DelScriptingProc { get; set; }

    public int? UpdScriptingProc { get; set; }

    public string? CustomScript { get; set; }

    public bool FireTriggersOnSnapshot { get; set; }
}
