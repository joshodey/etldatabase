﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Sysarticlecolumn
{
    public int Artid { get; set; }

    public int Colid { get; set; }

    public bool? IsUdt { get; set; }

    public bool? IsXml { get; set; }

    public bool? IsMax { get; set; }
}
