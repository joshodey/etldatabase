﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class ProfileName
{
    public int ProfileNameId { get; set; }

    public string Name { get; set; } = null!;

    public bool IsEnabled { get; set; }
}
