﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class SettlementSummary
{
    public DateTime SettleDate { get; set; }

    public string Code { get; set; } = null!;

    public decimal? Amount { get; set; }
}
