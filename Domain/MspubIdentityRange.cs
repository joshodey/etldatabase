﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MspubIdentityRange
{
    public int Objid { get; set; }

    public long Range { get; set; }

    public long PubRange { get; set; }

    public long CurrentPubRange { get; set; }

    public int Threshold { get; set; }

    public long? LastSeed { get; set; }
}
