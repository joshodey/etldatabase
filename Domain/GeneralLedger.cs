﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class GeneralLedger
{
    public int BankId { get; set; }

    public string AccountId { get; set; } = null!;

    public int TransactionId { get; set; }

    public decimal Debit { get; set; }

    public decimal Credit { get; set; }
}
