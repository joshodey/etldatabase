﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class BankAccount
{
    public int AccountId { get; set; }

    public int PartyId { get; set; }

    public int BankId { get; set; }

    public string BankCode { get; set; } = null!;

    public string AccountNumber { get; set; } = null!;

    public string AccountName { get; set; } = null!;

    public int SplitPercentage { get; set; }

    public bool IsDefault { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual ICollection<AccountChangeRequest> AccountChangeRequests { get; set; } = new List<AccountChangeRequest>();

    public virtual Bank Bank { get; set; } = null!;

    public virtual Party Party { get; set; } = null!;
}
