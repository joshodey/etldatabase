﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace EtlDataBase;

public partial class GatewayDbContext : DbContext
{
    public GatewayDbContext()
    {
    }

    public GatewayDbContext(DbContextOptions<GatewayDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Account> Accounts { get; set; }

    public virtual DbSet<AccountChangeRequest> AccountChangeRequests { get; set; }

    public virtual DbSet<AspNetRole> AspNetRoles { get; set; }

    public virtual DbSet<AspNetUser> AspNetUsers { get; set; }

    public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }

    public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }

    public virtual DbSet<AspNetUserRole> AspNetUserRoles { get; set; }

    public virtual DbSet<Audit> Audits { get; set; }

    public virtual DbSet<Authorization> Authorizations { get; set; }

    public virtual DbSet<Bank> Banks { get; set; }

    public virtual DbSet<BankAccount> BankAccounts { get; set; }

    public virtual DbSet<Brand> Brands { get; set; }

    public virtual DbSet<Card> Cards { get; set; }

    public virtual DbSet<CreateOrder> CreateOrders { get; set; }

    public virtual DbSet<Currency> Currencies { get; set; }

    public virtual DbSet<Customer> Customers { get; set; }

    public virtual DbSet<Department> Departments { get; set; }

    public virtual DbSet<DisputeMessage> DisputeMessages { get; set; }

    public virtual DbSet<DisputeResolution> DisputeResolutions { get; set; }

    public virtual DbSet<DocumentType> DocumentTypes { get; set; }

    public virtual DbSet<ElmahError> ElmahErrors { get; set; }

    public virtual DbSet<ErrorLog> ErrorLogs { get; set; }

    public virtual DbSet<Faq> Faqs { get; set; }

    public virtual DbSet<GeneralLedger> GeneralLedgers { get; set; }

    public virtual DbSet<IntegrationOption> IntegrationOptions { get; set; }

    public virtual DbSet<MerchantDetail> MerchantDetails { get; set; }

    public virtual DbSet<MessageLog> MessageLogs { get; set; }

    public virtual DbSet<MessageTemplate> MessageTemplates { get; set; }

    public virtual DbSet<MspeerConflictdetectionconfigrequest> MspeerConflictdetectionconfigrequests { get; set; }

    public virtual DbSet<MspeerConflictdetectionconfigresponse> MspeerConflictdetectionconfigresponses { get; set; }

    public virtual DbSet<MspeerLsn> MspeerLsns { get; set; }

    public virtual DbSet<MspeerOriginatoridHistory> MspeerOriginatoridHistories { get; set; }

    public virtual DbSet<MspeerRequest> MspeerRequests { get; set; }

    public virtual DbSet<MspeerResponse> MspeerResponses { get; set; }

    public virtual DbSet<MspeerTopologyrequest> MspeerTopologyrequests { get; set; }

    public virtual DbSet<MspeerTopologyresponse> MspeerTopologyresponses { get; set; }

    public virtual DbSet<MspubIdentityRange> MspubIdentityRanges { get; set; }

    public virtual DbSet<Notification> Notifications { get; set; }

    public virtual DbSet<Party> Parties { get; set; }

    public virtual DbSet<PartyCategory> PartyCategories { get; set; }

    public virtual DbSet<PartyDocumentType> PartyDocumentTypes { get; set; }

    public virtual DbSet<PartyType> PartyTypes { get; set; }

    public virtual DbSet<PartyTypeDocumentType> PartyTypeDocumentTypes { get; set; }

    public virtual DbSet<PasswordHistory> PasswordHistories { get; set; }

    public virtual DbSet<PasswordRecovery> PasswordRecoveries { get; set; }

    public virtual DbSet<PaymentInvoice> PaymentInvoices { get; set; }

    public virtual DbSet<PaymentPage> PaymentPages { get; set; }

    public virtual DbSet<PaymentPlan> PaymentPlans { get; set; }

    public virtual DbSet<PaymentSubscription> PaymentSubscriptions { get; set; }

    public virtual DbSet<Permission> Permissions { get; set; }

    public virtual DbSet<Preference> Preferences { get; set; }

    public virtual DbSet<Processor> Processors { get; set; }

    public virtual DbSet<Profile> Profiles { get; set; }

    public virtual DbSet<ProfileName> ProfileNames { get; set; }

    public virtual DbSet<Prospect> Prospects { get; set; }

    public virtual DbSet<RegistrationType> RegistrationTypes { get; set; }

    public virtual DbSet<Role> Roles { get; set; }

    public virtual DbSet<RoleParty> RoleParties { get; set; }

    public virtual DbSet<RolePermission> RolePermissions { get; set; }

    public virtual DbSet<Route> Routes { get; set; }

    public virtual DbSet<Setting> Settings { get; set; }

    public virtual DbSet<SettlementItem> SettlementItems { get; set; }

    public virtual DbSet<SettlementRule> SettlementRules { get; set; }

    public virtual DbSet<SettlementSummary> SettlementSummaries { get; set; }

    public virtual DbSet<Sysarticle> Sysarticles { get; set; }

    public virtual DbSet<Sysarticlecolumn> Sysarticlecolumns { get; set; }

    public virtual DbSet<Sysarticleupdate> Sysarticleupdates { get; set; }

    public virtual DbSet<Sysextendedarticlesview> Sysextendedarticlesviews { get; set; }

    public virtual DbSet<Syspublication> Syspublications { get; set; }

    public virtual DbSet<Sysreplserver> Sysreplservers { get; set; }

    public virtual DbSet<Sysschemaarticle> Sysschemaarticles { get; set; }

    public virtual DbSet<Syssubscription> Syssubscriptions { get; set; }

    public virtual DbSet<Systranschema> Systranschemas { get; set; }

    public virtual DbSet<Token> Tokens { get; set; }

    public virtual DbSet<Transaction> Transactions { get; set; }

    public virtual DbSet<Upload> Uploads { get; set; }

    public virtual DbSet<User> Users { get; set; }

    public virtual DbSet<UserParty> UserParties { get; set; }

    public virtual DbSet<UserRole> UserRoles { get; set; }

    public virtual DbSet<WorkflowActivation> WorkflowActivations { get; set; }

    public virtual DbSet<WorkflowActivationDetail> WorkflowActivationDetails { get; set; }

    public virtual DbSet<WorkflowActivationLog> WorkflowActivationLogs { get; set; }

    public virtual DbSet<WorkflowActivationRequirement> WorkflowActivationRequirements { get; set; }

    public virtual DbSet<WorkflowActivationStep> WorkflowActivationSteps { get; set; }

    public virtual DbSet<WorkflowActivationStepDetail> WorkflowActivationStepDetails { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=172.19.8.18,1700;Database=gateway_db;User Id=sa;Password=vnp-1234;TrustServerCertificate=True");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Account>(entity =>
        {
            entity.Property(e => e.Balance).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.CreditSms).HasColumnName("CreditSMS");
            entity.Property(e => e.CurrentOtp)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasColumnName("CurrentOTP");
            entity.Property(e => e.DebitSms).HasColumnName("DebitSMS");
            entity.Property(e => e.Email)
                .HasMaxLength(80)
                .IsUnicode(false);
            entity.Property(e => e.Lien).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.LiquidationStory)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.Otpexpires).HasColumnName("OTPExpires");
            entity.Property(e => e.Otpstreak).HasColumnName("OTPStreak");
            entity.Property(e => e.PasswordHash)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Phone)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.SuspendReason)
                .HasMaxLength(250)
                .IsUnicode(false);
        });

        modelBuilder.Entity<AccountChangeRequest>(entity =>
        {
            entity.HasIndex(e => e.AccountId, "IX_AccountChangeRequests_AccountID");

            entity.Property(e => e.AccountChangeRequestId).HasColumnName("AccountChangeRequestID");
            entity.Property(e => e.AccountId).HasColumnName("AccountID");
            entity.Property(e => e.UploadId).HasColumnName("UploadID");

            entity.HasOne(d => d.Account).WithMany(p => p.AccountChangeRequests).HasForeignKey(d => d.AccountId);
        });

        modelBuilder.Entity<AspNetRole>(entity =>
        {
            entity.Property(e => e.Id).HasMaxLength(128);
            entity.Property(e => e.Name).HasMaxLength(256);
        });

        modelBuilder.Entity<AspNetUser>(entity =>
        {
            entity.Property(e => e.Id).HasMaxLength(128);
            entity.Property(e => e.Address).HasMaxLength(250);
            entity.Property(e => e.Country).HasMaxLength(100);
            entity.Property(e => e.Email).HasMaxLength(256);
            entity.Property(e => e.UserName).HasMaxLength(256);
            entity.Property(e => e.ZipCode).HasMaxLength(50);
        });

        modelBuilder.Entity<AspNetUserClaim>(entity =>
        {
            entity.HasIndex(e => e.UserId, "IX_AspNetUserClaims_UserId");

            entity.Property(e => e.UserId).HasMaxLength(128);
        });

        modelBuilder.Entity<AspNetUserLogin>(entity =>
        {
            entity.HasKey(e => e.LoginProvider);

            entity.HasIndex(e => e.UserId, "IX_AspNetUserLogins_UserId");

            entity.Property(e => e.LoginProvider).HasMaxLength(128);
            entity.Property(e => e.ProviderKey).HasMaxLength(128);
            entity.Property(e => e.UserId).HasMaxLength(128);
        });

        modelBuilder.Entity<AspNetUserRole>(entity =>
        {
            entity.HasKey(e => new { e.UserId, e.RoleId });

            entity.HasIndex(e => e.RoleId, "IX_AspNetUserRoles_RoleId");

            entity.Property(e => e.UserId).HasMaxLength(128);
            entity.Property(e => e.RoleId).HasMaxLength(128);

            entity.HasOne(d => d.Role).WithMany(p => p.AspNetUserRoles).HasForeignKey(d => d.RoleId);
        });

        modelBuilder.Entity<Audit>(entity =>
        {
            entity.Property(e => e.AuditId).HasColumnName("AuditID");
            entity.Property(e => e.TableName)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Authorization>(entity =>
        {
            entity.Property(e => e.Action).HasMaxLength(50);
            entity.Property(e => e.Authorizer).HasMaxLength(50);
            entity.Property(e => e.EntityId).HasMaxLength(100);
            entity.Property(e => e.EntityType).HasMaxLength(50);
            entity.Property(e => e.Requester).HasMaxLength(50);
        });

        modelBuilder.Entity<Bank>(entity =>
        {
            entity.Property(e => e.BankId).HasColumnName("BankID");
            entity.Property(e => e.Description)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(5)
                .IsUnicode(false);
        });

        modelBuilder.Entity<BankAccount>(entity =>
        {
            entity.HasKey(e => e.AccountId);

            entity.HasIndex(e => e.BankId, "IX_BankAccounts_BankID");

            entity.HasIndex(e => e.PartyId, "IX_BankAccounts_PartyId");

            entity.Property(e => e.AccountId).HasColumnName("AccountID");
            entity.Property(e => e.AccountName)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.AccountNumber)
                .HasMaxLength(15)
                .IsUnicode(false);
            entity.Property(e => e.BankCode)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.BankId).HasColumnName("BankID");

            entity.HasOne(d => d.Bank).WithMany(p => p.BankAccounts).HasForeignKey(d => d.BankId);

            entity.HasOne(d => d.Party).WithMany(p => p.BankAccounts).HasForeignKey(d => d.PartyId);
        });

        modelBuilder.Entity<Brand>(entity =>
        {
            entity.Property(e => e.Logo).HasMaxLength(150);
            entity.Property(e => e.Name).HasMaxLength(50);
            entity.Property(e => e.ShortName).HasMaxLength(20);
        });

        modelBuilder.Entity<Card>(entity =>
        {
            entity.HasKey(e => e.Id)
                .HasName("Cards_pk")
                .IsClustered(false);

            entity.Property(e => e.Scheme).HasMaxLength(50);
            entity.Property(e => e.UniqueId).HasMaxLength(50);
        });

        modelBuilder.Entity<CreateOrder>(entity =>
        {
            entity.Property(e => e.CreateOrderId).HasColumnName("CreateOrderID");
            entity.Property(e => e.Amount).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.TransactionId).HasColumnName("TransactionID");
        });

        modelBuilder.Entity<Currency>(entity =>
        {
            entity.Property(e => e.CurrencyId).HasColumnName("CurrencyID");
            entity.Property(e => e.Symbol).HasColumnName("symbol");
        });

        modelBuilder.Entity<Customer>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_Customers_PartyId");

            entity.Property(e => e.CustomerId).HasColumnName("CustomerID");
            entity.Property(e => e.Address)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.CustomerCode)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.DisplayName)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.EmailAddress)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.FirstName)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.LastName)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.PhoneNumber)
                .HasMaxLength(15)
                .IsUnicode(false);
            entity.Property(e => e.TotalSpend).HasColumnType("decimal(18, 2)");

            entity.HasOne(d => d.Party).WithMany(p => p.Customers).HasForeignKey(d => d.PartyId);
        });

        modelBuilder.Entity<Department>(entity =>
        {
            entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");
            entity.Property(e => e.Description)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(30)
                .IsUnicode(false);
        });

        modelBuilder.Entity<DisputeMessage>(entity =>
        {
            entity.HasIndex(e => e.DisputeResolutionId, "IX_DisputeMessages_DisputeResolutionID");

            entity.Property(e => e.DisputeMessageId).HasColumnName("DisputeMessageID");
            entity.Property(e => e.DisputeResolutionId).HasColumnName("DisputeResolutionID");
            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Message).IsUnicode(false);

            entity.HasOne(d => d.DisputeResolution).WithMany(p => p.DisputeMessages).HasForeignKey(d => d.DisputeResolutionId);
        });

        modelBuilder.Entity<DisputeResolution>(entity =>
        {
            entity.Property(e => e.DisputeResolutionId).HasColumnName("DisputeResolutionID");
            entity.Property(e => e.Message).IsUnicode(false);
            entity.Property(e => e.ReferenceId).HasColumnName("ReferenceID");
            entity.Property(e => e.ResponderUserId).HasColumnName("ResponderUserID");
            entity.Property(e => e.Title)
                .HasMaxLength(200)
                .IsUnicode(false);
        });

        modelBuilder.Entity<DocumentType>(entity =>
        {
            entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentTypeID");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ElmahError>(entity =>
        {
            entity.HasKey(e => e.ErrorId);

            entity.ToTable("ELMAH_Error");

            entity.Property(e => e.ErrorId).HasDefaultValueSql("(newid())");
            entity.Property(e => e.AllXml).HasColumnType("ntext");
            entity.Property(e => e.Application).HasMaxLength(60);
            entity.Property(e => e.Host).HasMaxLength(50);
            entity.Property(e => e.Message).HasMaxLength(500);
            entity.Property(e => e.Source).HasMaxLength(60);
            entity.Property(e => e.Type).HasMaxLength(100);
            entity.Property(e => e.User).HasMaxLength(50);
        });

        modelBuilder.Entity<ErrorLog>(entity =>
        {
            entity.ToTable("ErrorLog");

            entity.Property(e => e.Message).HasMaxLength(250);
            entity.Property(e => e.RequestAddress).HasMaxLength(150);
        });

        modelBuilder.Entity<Faq>(entity =>
        {
            entity.Property(e => e.FaqId).HasColumnName("FaqID");
            entity.Property(e => e.Answer)
                .HasMaxLength(1000)
                .IsUnicode(false);
            entity.Property(e => e.Question)
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<GeneralLedger>(entity =>
        {
            entity.HasKey(e => e.BankId);

            entity.ToTable("GeneralLedger");

            entity.Property(e => e.BankId).HasColumnName("BankID");
            entity.Property(e => e.AccountId)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.Credit).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.Debit).HasColumnType("decimal(18, 2)");
        });

        modelBuilder.Entity<IntegrationOption>(entity =>
        {
            entity.Property(e => e.IntegrationOptionId).HasColumnName("IntegrationOptionID");
            entity.Property(e => e.Icon).HasMaxLength(50);
            entity.Property(e => e.IntegrationType).HasMaxLength(12);
            entity.Property(e => e.Name).HasMaxLength(50);
            entity.Property(e => e.Url)
                .HasMaxLength(255)
                .HasColumnName("URL");
        });

        modelBuilder.Entity<MerchantDetail>(entity =>
        {
            entity.Property(e => e.Code)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.SecretKey)
                .HasMaxLength(250)
                .IsUnicode(false);
        });

        modelBuilder.Entity<MessageLog>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__MessageL__3214EC07E7579DB8");

            entity.Property(e => e.Direction).HasMaxLength(50);
            entity.Property(e => e.RemoteEndpoint).HasMaxLength(300);
            entity.Property(e => e.Service).HasMaxLength(50);
            entity.Property(e => e.Timestamp).HasDefaultValueSql("(getdate())");

            entity.HasOne(d => d.Transaction).WithMany(p => p.MessageLogs)
                .HasForeignKey(d => d.TransactionId)
                .HasConstraintName("FK__MessageLo__Trans__5070F446");
        });

        modelBuilder.Entity<MessageTemplate>(entity =>
        {
            entity.HasKey(e => e.MessageId);

            entity.Property(e => e.MessageId).HasColumnName("MessageID");
            entity.Property(e => e.ClientCopy).IsUnicode(false);
            entity.Property(e => e.DashboardMessage).IsUnicode(false);
            entity.Property(e => e.Description)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Message1).IsUnicode(false);
            entity.Property(e => e.Message2).IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Subject).IsUnicode(false);
        });

        modelBuilder.Entity<MspeerConflictdetectionconfigrequest>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__MSpeer_c__3213E83FBFD638EE");

            entity.ToTable("MSpeer_conflictdetectionconfigrequest");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ModifiedDate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.PhaseTimedOut).HasColumnName("phase_timed_out");
            entity.Property(e => e.ProgressPhase)
                .HasMaxLength(32)
                .HasColumnName("progress_phase");
            entity.Property(e => e.Publication)
                .HasMaxLength(128)
                .HasColumnName("publication");
            entity.Property(e => e.SentDate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("sent_date");
            entity.Property(e => e.Timeout).HasColumnName("timeout");
        });

        modelBuilder.Entity<MspeerConflictdetectionconfigresponse>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("MSpeer_conflictdetectionconfigresponse");

            entity.HasIndex(e => new { e.RequestId, e.PeerNode, e.PeerDb }, "uci_MSpeer_conflictdetectionconfigresponse")
                .IsUnique()
                .IsClustered();

            entity.Property(e => e.ConflictdetectionEnabled).HasColumnName("conflictdetection_enabled");
            entity.Property(e => e.IsPeer).HasColumnName("is_peer");
            entity.Property(e => e.ModifiedDate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OriginatorId).HasColumnName("originator_id");
            entity.Property(e => e.PeerConflictRetention).HasColumnName("peer_conflict_retention");
            entity.Property(e => e.PeerContinueOnconflict).HasColumnName("peer_continue_onconflict");
            entity.Property(e => e.PeerDb)
                .HasMaxLength(128)
                .HasColumnName("peer_db");
            entity.Property(e => e.PeerDbVersion).HasColumnName("peer_db_version");
            entity.Property(e => e.PeerNode)
                .HasMaxLength(128)
                .HasColumnName("peer_node");
            entity.Property(e => e.PeerSubscriptions)
                .HasColumnType("xml")
                .HasColumnName("peer_subscriptions");
            entity.Property(e => e.PeerVersion).HasColumnName("peer_version");
            entity.Property(e => e.ProgressPhase)
                .HasMaxLength(32)
                .HasColumnName("progress_phase");
            entity.Property(e => e.RequestId).HasColumnName("request_id");
        });

        modelBuilder.Entity<MspeerLsn>(entity =>
        {
            entity.HasKey(e => e.Id)
                .HasName("PK__MSpeer_l__3213E83E19874677")
                .IsClustered(false);

            entity.ToTable("MSpeer_lsns");

            entity.HasIndex(e => e.OriginatorPublication, "nci_MSpeer_lsns");

            entity.HasIndex(e => new { e.Originator, e.OriginatorDb, e.OriginatorPublicationId, e.OriginatorDbVersion, e.OriginatorLsn }, "uci_MSpeer_lsns")
                .IsUnique()
                .IsClustered();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.LastUpdated)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("last_updated");
            entity.Property(e => e.Originator)
                .HasMaxLength(128)
                .HasColumnName("originator");
            entity.Property(e => e.OriginatorDb)
                .HasMaxLength(128)
                .HasColumnName("originator_db");
            entity.Property(e => e.OriginatorDbVersion).HasColumnName("originator_db_version");
            entity.Property(e => e.OriginatorId).HasColumnName("originator_id");
            entity.Property(e => e.OriginatorLsn)
                .HasMaxLength(16)
                .HasColumnName("originator_lsn");
            entity.Property(e => e.OriginatorPublication)
                .HasMaxLength(128)
                .HasColumnName("originator_publication");
            entity.Property(e => e.OriginatorPublicationId).HasColumnName("originator_publication_id");
            entity.Property(e => e.OriginatorVersion).HasColumnName("originator_version");
        });

        modelBuilder.Entity<MspeerOriginatoridHistory>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("MSpeer_originatorid_history");

            entity.HasIndex(e => new { e.OriginatorPublication, e.OriginatorId, e.OriginatorNode, e.OriginatorDb, e.OriginatorDbVersion }, "uci_MSpeer_originatorid_history")
                .IsUnique()
                .IsClustered();

            entity.Property(e => e.InsertedDate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("inserted_date");
            entity.Property(e => e.OriginatorDb)
                .HasMaxLength(128)
                .HasColumnName("originator_db");
            entity.Property(e => e.OriginatorDbVersion).HasColumnName("originator_db_version");
            entity.Property(e => e.OriginatorId).HasColumnName("originator_id");
            entity.Property(e => e.OriginatorNode)
                .HasMaxLength(128)
                .HasColumnName("originator_node");
            entity.Property(e => e.OriginatorPublication)
                .HasMaxLength(128)
                .HasColumnName("originator_publication");
            entity.Property(e => e.OriginatorVersion).HasColumnName("originator_version");
        });

        modelBuilder.Entity<MspeerRequest>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("MSpeer_request");

            entity.Property(e => e.Description)
                .HasMaxLength(4000)
                .HasColumnName("description");
            entity.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasColumnName("id");
            entity.Property(e => e.Publication)
                .HasMaxLength(128)
                .HasColumnName("publication");
            entity.Property(e => e.SentDate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("sent_date");
        });

        modelBuilder.Entity<MspeerResponse>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("MSpeer_response");

            entity.Property(e => e.Peer)
                .HasMaxLength(128)
                .HasColumnName("peer");
            entity.Property(e => e.PeerDb)
                .HasMaxLength(128)
                .HasColumnName("peer_db");
            entity.Property(e => e.ReceivedDate)
                .HasColumnType("datetime")
                .HasColumnName("received_date");
            entity.Property(e => e.RequestId).HasColumnName("request_id");
        });

        modelBuilder.Entity<MspeerTopologyrequest>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("MSpeer_topologyrequest");

            entity.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasColumnName("id");
            entity.Property(e => e.Publication)
                .HasMaxLength(128)
                .HasColumnName("publication");
            entity.Property(e => e.SentDate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("sent_date");
        });

        modelBuilder.Entity<MspeerTopologyresponse>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("MSpeer_topologyresponse");

            entity.Property(e => e.ConnectionInfo)
                .HasColumnType("xml")
                .HasColumnName("connection_info");
            entity.Property(e => e.OriginatorId).HasColumnName("originator_id");
            entity.Property(e => e.Peer)
                .HasMaxLength(128)
                .HasColumnName("peer");
            entity.Property(e => e.PeerConflictRetention).HasColumnName("peer_conflict_retention");
            entity.Property(e => e.PeerDb)
                .HasMaxLength(128)
                .HasColumnName("peer_db");
            entity.Property(e => e.PeerVersion).HasColumnName("peer_version");
            entity.Property(e => e.ReceivedDate)
                .HasColumnType("datetime")
                .HasColumnName("received_date");
            entity.Property(e => e.RequestId).HasColumnName("request_id");
        });

        modelBuilder.Entity<MspubIdentityRange>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("MSpub_identity_range");

            entity.HasIndex(e => e.Objid, "unc1MSpub_identity_range").IsUnique();

            entity.Property(e => e.CurrentPubRange).HasColumnName("current_pub_range");
            entity.Property(e => e.LastSeed).HasColumnName("last_seed");
            entity.Property(e => e.Objid).HasColumnName("objid");
            entity.Property(e => e.PubRange).HasColumnName("pub_range");
            entity.Property(e => e.Range).HasColumnName("range");
            entity.Property(e => e.Threshold).HasColumnName("threshold");
        });

        modelBuilder.Entity<Notification>(entity =>
        {
            entity.HasIndex(e => e.MessageId, "IX_Notifications_MessageID");

            entity.Property(e => e.NotificationId).HasColumnName("NotificationID");
            entity.Property(e => e.MessageId).HasColumnName("MessageID");
            entity.Property(e => e.OptionalParameters).IsUnicode(false);
            entity.Property(e => e.Recipient)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.SenderDisplayName)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.SenderEmail)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Url)
                .HasMaxLength(1000)
                .IsUnicode(false)
                .HasColumnName("URL");
            entity.Property(e => e.UserId).HasColumnName("UserID");

            entity.HasOne(d => d.Message).WithMany(p => p.Notifications).HasForeignKey(d => d.MessageId);
        });

        modelBuilder.Entity<Party>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Parties__3214EC0721B6055D");

            entity.HasIndex(e => e.Name, "UQ__Parties__737584F6CD1B8993").IsUnique();

            entity.HasIndex(e => e.Code, "UQ__Parties__A25C5AA724927208").IsUnique();

            entity.Property(e => e.Account).HasMaxLength(25);
            entity.Property(e => e.AddVat).HasColumnName("AddVAT");
            entity.Property(e => e.AmexId)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Bank).HasMaxLength(150);
            entity.Property(e => e.CardFee).HasMaxLength(5);
            entity.Property(e => e.CardOnFileId).HasMaxLength(100);
            entity.Property(e => e.Code).HasMaxLength(150);
            entity.Property(e => e.Discriminator)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Email).HasMaxLength(150);
            entity.Property(e => e.EncryptionIv)
                .HasMaxLength(250)
                .IsUnicode(false)
                .HasColumnName("EncryptionIV");
            entity.Property(e => e.EncryptionKey)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.Ipaddresses)
                .HasMaxLength(160)
                .HasColumnName("IPAddresses");
            entity.Property(e => e.IsRecurring).HasColumnName("isRecurring");
            entity.Property(e => e.MaxFee).HasColumnType("decimal(18, 0)");
            entity.Property(e => e.MerId)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.MerchantId)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Name).HasMaxLength(150);
            entity.Property(e => e.PayattitudeFee).HasMaxLength(5);
            entity.Property(e => e.ProfileName)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.RetailerId).HasMaxLength(50);
            entity.Property(e => e.SecretKey)
                .HasMaxLength(500)
                .IsUnicode(false);
            entity.Property(e => e.TerminalId)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Url).HasMaxLength(500);
            entity.Property(e => e.Vat)
                .HasColumnType("decimal(18, 4)")
                .HasColumnName("VAT");

            entity.HasOne(d => d.Profile).WithMany(p => p.Parties)
                .HasForeignKey(d => d.ProfileId)
                .HasConstraintName("FK__Parties__Profile__5165187F");
        });

        modelBuilder.Entity<PartyCategory>(entity =>
        {
            entity.Property(e => e.PartyCategoryId).HasColumnName("PartyCategoryID");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.IconName)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<PartyDocumentType>(entity =>
        {
            entity.Property(e => e.PartyDocumentTypeId).HasColumnName("PartyDocumentTypeID");
            entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentTypeID");
            entity.Property(e => e.UploadId).HasColumnName("UploadID");
        });

        modelBuilder.Entity<PartyType>(entity =>
        {
            entity.Property(e => e.PartyTypeId).HasColumnName("PartyTypeID");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.IconName)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<PartyTypeDocumentType>(entity =>
        {
            entity.HasIndex(e => e.DocumentTypeId, "IX_PartyTypeDocumentTypes_DocumentTypeID");

            entity.HasIndex(e => e.PartyTypeId, "IX_PartyTypeDocumentTypes_PartyTypeID");

            entity.Property(e => e.PartyTypeDocumentTypeId).HasColumnName("PartyTypeDocumentTypeID");
            entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentTypeID");
            entity.Property(e => e.PartyTypeId).HasColumnName("PartyTypeID");

            entity.HasOne(d => d.DocumentType).WithMany(p => p.PartyTypeDocumentTypes).HasForeignKey(d => d.DocumentTypeId);

            entity.HasOne(d => d.PartyType).WithMany(p => p.PartyTypeDocumentTypes).HasForeignKey(d => d.PartyTypeId);
        });

        modelBuilder.Entity<PasswordHistory>(entity =>
        {
            entity.Property(e => e.PasswordHistoryId).HasColumnName("PasswordHistoryID");
            entity.Property(e => e.EmailAddress)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Password)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PasswordSalt)
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<PasswordRecovery>(entity =>
        {
            entity.Property(e => e.PasswordRecoveryId).HasColumnName("PasswordRecoveryID");
            entity.Property(e => e.EmailAddress)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.SecurityCode)
                .HasMaxLength(250)
                .IsUnicode(false);
        });

        modelBuilder.Entity<PaymentInvoice>(entity =>
        {
            entity.HasNoKey();

            entity.Property(e => e.AdditionalField1).HasColumnName("additionalField1");
            entity.Property(e => e.AdditionalField2).HasColumnName("additionalField2");
            entity.Property(e => e.Amount)
                .HasColumnType("decimal(18, 2)")
                .HasColumnName("amount");
            entity.Property(e => e.Customerinfo).HasColumnName("customerinfo");
            entity.Property(e => e.Customername)
                .HasMaxLength(50)
                .HasColumnName("customername");
            entity.Property(e => e.Email)
                .HasMaxLength(75)
                .HasColumnName("email");
            entity.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasColumnName("id");
            entity.Property(e => e.Invoicenumber)
                .HasMaxLength(50)
                .HasColumnName("invoicenumber");
            entity.Property(e => e.MerchantId).HasColumnName("merchantId");
            entity.Property(e => e.Notetocustomer).HasColumnName("notetocustomer");
            entity.Property(e => e.Status)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("status");
            entity.Property(e => e.Transactionid).HasColumnName("transactionid");
            entity.Property(e => e.Url)
                .HasMaxLength(100)
                .HasColumnName("url");
        });

        modelBuilder.Entity<PaymentPage>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_PaymentPages_PartyId");

            entity.HasIndex(e => e.PaymentPlanId, "IX_PaymentPages_PaymentPlanID");

            entity.Property(e => e.PaymentPageId).HasColumnName("PaymentPageID");
            entity.Property(e => e.Amount).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.PageDescription)
                .HasMaxLength(1000)
                .IsUnicode(false);
            entity.Property(e => e.PageName)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.PaymentPlanId).HasColumnName("PaymentPlanID");
            entity.Property(e => e.PlanTypeId).HasColumnName("PlanTypeID");

            entity.HasOne(d => d.Party).WithMany(p => p.PaymentPages).HasForeignKey(d => d.PartyId);

            entity.HasOne(d => d.PaymentPlan).WithMany(p => p.PaymentPages).HasForeignKey(d => d.PaymentPlanId);
        });

        modelBuilder.Entity<PaymentPlan>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_PaymentPlans_PartyId");

            entity.Property(e => e.PaymentPlanId).HasColumnName("PaymentPlanID");
            entity.Property(e => e.PlanAmount).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.PlanDescription).IsUnicode(false);
            entity.Property(e => e.PlanName)
                .HasMaxLength(50)
                .IsUnicode(false);

            entity.HasOne(d => d.Party).WithMany(p => p.PaymentPlans).HasForeignKey(d => d.PartyId);
        });

        modelBuilder.Entity<PaymentSubscription>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_PaymentSubscriptions_PartyId");

            entity.Property(e => e.PaymentSubscriptionId).HasColumnName("PaymentSubscriptionID");
            entity.Property(e => e.Amount).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.CustomerEmail)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.CustomerName)
                .HasMaxLength(50)
                .IsUnicode(false);

            entity.HasOne(d => d.Party).WithMany(p => p.PaymentSubscriptions).HasForeignKey(d => d.PartyId);
        });

        modelBuilder.Entity<Permission>(entity =>
        {
            entity.Property(e => e.PermissionId)
                .ValueGeneratedNever()
                .HasColumnName("PermissionID");
            entity.Property(e => e.Description)
                .HasMaxLength(1000)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Preference>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_Preferences_PartyId").IsUnique();

            entity.Property(e => e.PreferenceId).HasColumnName("PreferenceID");
            entity.Property(e => e.AllowQrpayment).HasColumnName("AllowQRPayment");
            entity.Property(e => e.AllowUssdpayment).HasColumnName("AllowUSSDPayment");
            entity.Property(e => e.EnabledOtpconfirmation).HasColumnName("EnabledOTPConfirmation");
            entity.Property(e => e.SendOtptoEmail).HasColumnName("SendOTPToEmail");

            entity.HasOne(d => d.Party).WithOne(p => p.Preference).HasForeignKey<Preference>(d => d.PartyId);
        });

        modelBuilder.Entity<Processor>(entity =>
        {
            entity.Property(e => e.Code).HasMaxLength(20);
            entity.Property(e => e.Name).HasMaxLength(50);
        });

        modelBuilder.Entity<Profile>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Profiles__3214EC07E1F16C7F");

            entity.Property(e => e.Name).HasMaxLength(50);
        });

        modelBuilder.Entity<ProfileName>(entity =>
        {
            entity.HasKey(e => e.ProfileNameId).HasName("PK__ProfileN__C55FE7BDBEC00601");

            entity.HasIndex(e => e.Name, "UQ__ProfileN__737584F653356C3A").IsUnique();

            entity.Property(e => e.ProfileNameId).HasColumnName("ProfileNameID");
            entity.Property(e => e.Name).HasMaxLength(50);
        });

        modelBuilder.Entity<Prospect>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Table__3214EC079A653C1E");

            entity.Property(e => e.AccountName).HasMaxLength(100);
            entity.Property(e => e.AccountNumber).HasMaxLength(10);
            entity.Property(e => e.AccountType).HasMaxLength(25);
            entity.Property(e => e.Address).HasMaxLength(250);
            entity.Property(e => e.Bank).HasMaxLength(50);
            entity.Property(e => e.Code)
                .HasMaxLength(160)
                .IsFixedLength();
            entity.Property(e => e.Email).HasMaxLength(150);
            entity.Property(e => e.Identity).HasMaxLength(100);
            entity.Property(e => e.Ipaddresses)
                .HasMaxLength(160)
                .HasColumnName("IPAddresses");
            entity.Property(e => e.MerId)
                .HasMaxLength(50)
                .IsFixedLength();
            entity.Property(e => e.MerchantId).HasMaxLength(100);
            entity.Property(e => e.Name).HasMaxLength(100);
            entity.Property(e => e.ProfileName).HasMaxLength(100);
            entity.Property(e => e.RetailerId).HasMaxLength(50);
            entity.Property(e => e.Status).HasMaxLength(100);
            entity.Property(e => e.Telephone).HasMaxLength(20);
            entity.Property(e => e.TerminalId)
                .HasMaxLength(50)
                .IsFixedLength();
            entity.Property(e => e.Url).HasMaxLength(50);
            entity.Property(e => e.Website).HasMaxLength(150);
        });

        modelBuilder.Entity<RegistrationType>(entity =>
        {
            entity.Property(e => e.RegistrationTypeId).HasColumnName("RegistrationTypeID");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Role>(entity =>
        {
            entity.Property(e => e.RoleId).HasColumnName("RoleID");
            entity.Property(e => e.Description)
                .HasMaxLength(1000)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<RoleParty>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_RoleParties_PartyId");

            entity.HasIndex(e => e.RoleId, "IX_RoleParties_RoleID");

            entity.HasIndex(e => e.UserId, "IX_RoleParties_UserID");

            entity.Property(e => e.RolePartyId).HasColumnName("RolePartyID");
            entity.Property(e => e.RoleId).HasColumnName("RoleID");
            entity.Property(e => e.UserId).HasColumnName("UserID");

            entity.HasOne(d => d.Party).WithMany(p => p.RoleParties).HasForeignKey(d => d.PartyId);

            entity.HasOne(d => d.Role).WithMany(p => p.RoleParties).HasForeignKey(d => d.RoleId);

            entity.HasOne(d => d.User).WithMany(p => p.RoleParties).HasForeignKey(d => d.UserId);
        });

        modelBuilder.Entity<RolePermission>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_RolePermissions_PartyId");

            entity.HasIndex(e => e.PermissionId, "IX_RolePermissions_PermissionID");

            entity.HasIndex(e => e.RoleId, "IX_RolePermissions_RoleID");

            entity.Property(e => e.RolePermissionId).HasColumnName("RolePermissionID");
            entity.Property(e => e.PermissionId).HasColumnName("PermissionID");
            entity.Property(e => e.RoleId).HasColumnName("RoleID");

            entity.HasOne(d => d.Party).WithMany(p => p.RolePermissions).HasForeignKey(d => d.PartyId);

            entity.HasOne(d => d.Permission).WithMany(p => p.RolePermissions).HasForeignKey(d => d.PermissionId);

            entity.HasOne(d => d.Role).WithMany(p => p.RolePermissions).HasForeignKey(d => d.RoleId);
        });

        modelBuilder.Entity<Route>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Routes__3214EC073D5E1FD2");

            entity.HasIndex(e => new { e.ProfileId, e.BrandName }, "UX_Routes").IsUnique();

            entity.Property(e => e.BrandName).HasMaxLength(20);
            entity.Property(e => e.FeeAmount).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.FeeType)
                .HasMaxLength(1)
                .HasDefaultValueSql("('%')")
                .IsFixedLength();
            entity.Property(e => e.ProcessorCode).HasMaxLength(20);

            entity.HasOne(d => d.Profile).WithMany(p => p.Routes)
                .HasForeignKey(d => d.ProfileId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Routes__ProfileI__5441852A");
        });

        modelBuilder.Entity<Setting>(entity =>
        {
            entity.HasNoKey();

            entity.Property(e => e.DateUpdated).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(250);
            entity.Property(e => e.Key).HasMaxLength(100);
            entity.Property(e => e.Value).HasMaxLength(250);
        });

        modelBuilder.Entity<SettlementItem>(entity =>
        {
            entity.HasKey(e => new { e.PartyId, e.TransactionId });

            entity.Property(e => e.Amount).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.SettleDate).HasColumnType("date");

            entity.HasOne(d => d.Party).WithMany(p => p.SettlementItems)
                .HasForeignKey(d => d.PartyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Settlemen__Party__5535A963");

            entity.HasOne(d => d.Transaction).WithMany(p => p.SettlementItems)
                .HasForeignKey(d => d.TransactionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Settlemen__Trans__5629CD9C");
        });

        modelBuilder.Entity<SettlementRule>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Settleme__3214EC07AC154E70");

            entity.HasIndex(e => new { e.RouteId, e.PartyId }, "UX_SettlementRules").IsUnique();

            entity.Property(e => e.Type)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Value).HasColumnType("decimal(18, 2)");

            entity.HasOne(d => d.Party).WithMany(p => p.SettlementRules)
                .HasForeignKey(d => d.PartyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Settlemen__Party__571DF1D5");

            entity.HasOne(d => d.Route).WithMany(p => p.SettlementRules)
                .HasForeignKey(d => d.RouteId)
                .HasConstraintName("FK__Settlemen__Route__5812160E");
        });

        modelBuilder.Entity<SettlementSummary>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("SettlementSummary");

            entity.Property(e => e.Amount).HasColumnType("decimal(38, 2)");
            entity.Property(e => e.Code).HasMaxLength(150);
            entity.Property(e => e.SettleDate).HasColumnType("date");
        });

        modelBuilder.Entity<Sysarticle>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("sysarticles");

            entity.HasIndex(e => new { e.Artid, e.Pubid }, "c1sysarticles")
                .IsUnique()
                .IsClustered();

            entity.Property(e => e.Artid)
                .ValueGeneratedOnAdd()
                .HasColumnName("artid");
            entity.Property(e => e.CreationScript)
                .HasMaxLength(255)
                .HasColumnName("creation_script");
            entity.Property(e => e.CustomScript)
                .HasMaxLength(2048)
                .HasColumnName("custom_script");
            entity.Property(e => e.DelCmd)
                .HasMaxLength(255)
                .HasColumnName("del_cmd");
            entity.Property(e => e.DelScriptingProc).HasColumnName("del_scripting_proc");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
            entity.Property(e => e.DestOwner)
                .HasMaxLength(128)
                .HasColumnName("dest_owner");
            entity.Property(e => e.DestTable)
                .HasMaxLength(128)
                .HasColumnName("dest_table");
            entity.Property(e => e.Filter).HasColumnName("filter");
            entity.Property(e => e.FilterClause)
                .HasColumnType("ntext")
                .HasColumnName("filter_clause");
            entity.Property(e => e.FireTriggersOnSnapshot).HasColumnName("fire_triggers_on_snapshot");
            entity.Property(e => e.InsCmd)
                .HasMaxLength(255)
                .HasColumnName("ins_cmd");
            entity.Property(e => e.InsScriptingProc).HasColumnName("ins_scripting_proc");
            entity.Property(e => e.Name)
                .HasMaxLength(128)
                .HasColumnName("name");
            entity.Property(e => e.Objid).HasColumnName("objid");
            entity.Property(e => e.PreCreationCmd).HasColumnName("pre_creation_cmd");
            entity.Property(e => e.Pubid).HasColumnName("pubid");
            entity.Property(e => e.SchemaOption)
                .HasMaxLength(8)
                .IsFixedLength()
                .HasColumnName("schema_option");
            entity.Property(e => e.Status).HasColumnName("status");
            entity.Property(e => e.SyncObjid).HasColumnName("sync_objid");
            entity.Property(e => e.Type).HasColumnName("type");
            entity.Property(e => e.UpdCmd)
                .HasMaxLength(255)
                .HasColumnName("upd_cmd");
            entity.Property(e => e.UpdScriptingProc).HasColumnName("upd_scripting_proc");
        });

        modelBuilder.Entity<Sysarticlecolumn>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("sysarticlecolumns");

            entity.HasIndex(e => new { e.Artid, e.Colid }, "idx_sysarticlecolumns")
                .IsUnique()
                .IsClustered();

            entity.Property(e => e.Artid).HasColumnName("artid");
            entity.Property(e => e.Colid).HasColumnName("colid");
            entity.Property(e => e.IsMax)
                .HasDefaultValueSql("((0))")
                .HasColumnName("is_max");
            entity.Property(e => e.IsUdt)
                .HasDefaultValueSql("((0))")
                .HasColumnName("is_udt");
            entity.Property(e => e.IsXml)
                .HasDefaultValueSql("((0))")
                .HasColumnName("is_xml");
        });

        modelBuilder.Entity<Sysarticleupdate>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("sysarticleupdates");

            entity.HasIndex(e => new { e.Artid, e.Pubid }, "unc1sysarticleupdates").IsUnique();

            entity.Property(e => e.Artid).HasColumnName("artid");
            entity.Property(e => e.Autogen).HasColumnName("autogen");
            entity.Property(e => e.ConflictTableid).HasColumnName("conflict_tableid");
            entity.Property(e => e.IdentitySupport).HasColumnName("identity_support");
            entity.Property(e => e.InsConflictProc).HasColumnName("ins_conflict_proc");
            entity.Property(e => e.Pubid).HasColumnName("pubid");
            entity.Property(e => e.SyncDelProc).HasColumnName("sync_del_proc");
            entity.Property(e => e.SyncInsProc).HasColumnName("sync_ins_proc");
            entity.Property(e => e.SyncUpdProc).HasColumnName("sync_upd_proc");
            entity.Property(e => e.SyncUpdTrig).HasColumnName("sync_upd_trig");
        });

        modelBuilder.Entity<Sysextendedarticlesview>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("sysextendedarticlesview");

            entity.Property(e => e.Artid).HasColumnName("artid");
            entity.Property(e => e.CreationScript)
                .HasMaxLength(255)
                .HasColumnName("creation_script");
            entity.Property(e => e.CustomScript)
                .HasMaxLength(2048)
                .HasColumnName("custom_script");
            entity.Property(e => e.DelCmd)
                .HasMaxLength(255)
                .HasColumnName("del_cmd");
            entity.Property(e => e.DelScriptingProc).HasColumnName("del_scripting_proc");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
            entity.Property(e => e.DestOwner)
                .HasMaxLength(128)
                .HasColumnName("dest_owner");
            entity.Property(e => e.DestTable)
                .HasMaxLength(128)
                .HasColumnName("dest_table");
            entity.Property(e => e.Filter).HasColumnName("filter");
            entity.Property(e => e.FilterClause)
                .HasColumnType("ntext")
                .HasColumnName("filter_clause");
            entity.Property(e => e.FireTriggersOnSnapshot).HasColumnName("fire_triggers_on_snapshot");
            entity.Property(e => e.InsCmd)
                .HasMaxLength(255)
                .HasColumnName("ins_cmd");
            entity.Property(e => e.InsScriptingProc).HasColumnName("ins_scripting_proc");
            entity.Property(e => e.Name)
                .HasMaxLength(128)
                .HasColumnName("name");
            entity.Property(e => e.Objid).HasColumnName("objid");
            entity.Property(e => e.PreCreationCmd).HasColumnName("pre_creation_cmd");
            entity.Property(e => e.Pubid).HasColumnName("pubid");
            entity.Property(e => e.SchemaOption)
                .HasMaxLength(8)
                .IsFixedLength()
                .HasColumnName("schema_option");
            entity.Property(e => e.Status).HasColumnName("status");
            entity.Property(e => e.SyncObjid).HasColumnName("sync_objid");
            entity.Property(e => e.Type).HasColumnName("type");
            entity.Property(e => e.UpdCmd)
                .HasMaxLength(255)
                .HasColumnName("upd_cmd");
            entity.Property(e => e.UpdScriptingProc).HasColumnName("upd_scripting_proc");
        });

        modelBuilder.Entity<Syspublication>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("syspublications");

            entity.HasIndex(e => e.Status, "nc3syspublications");

            entity.HasIndex(e => e.Pubid, "uc1syspublications")
                .IsUnique()
                .IsClustered();

            entity.HasIndex(e => e.Name, "unc2syspublications").IsUnique();

            entity.Property(e => e.AdGuidname)
                .HasMaxLength(128)
                .HasColumnName("ad_guidname");
            entity.Property(e => e.AllowAnonymous).HasColumnName("allow_anonymous");
            entity.Property(e => e.AllowDts).HasColumnName("allow_dts");
            entity.Property(e => e.AllowInitializeFromBackup).HasColumnName("allow_initialize_from_backup");
            entity.Property(e => e.AllowPull).HasColumnName("allow_pull");
            entity.Property(e => e.AllowPush).HasColumnName("allow_push");
            entity.Property(e => e.AllowQueuedTran).HasColumnName("allow_queued_tran");
            entity.Property(e => e.AllowSubscriptionCopy).HasColumnName("allow_subscription_copy");
            entity.Property(e => e.AllowSyncTran).HasColumnName("allow_sync_tran");
            entity.Property(e => e.AltSnapshotFolder)
                .HasMaxLength(255)
                .HasColumnName("alt_snapshot_folder");
            entity.Property(e => e.AutogenSyncProcs).HasColumnName("autogen_sync_procs");
            entity.Property(e => e.BackwardCompLevel)
                .HasDefaultValueSql("((10))")
                .HasColumnName("backward_comp_level");
            entity.Property(e => e.CentralizedConflicts).HasColumnName("centralized_conflicts");
            entity.Property(e => e.CompressSnapshot).HasColumnName("compress_snapshot");
            entity.Property(e => e.ConflictPolicy).HasColumnName("conflict_policy");
            entity.Property(e => e.ConflictRetention).HasColumnName("conflict_retention");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
            entity.Property(e => e.EnabledForInternet).HasColumnName("enabled_for_internet");
            entity.Property(e => e.FtpAddress)
                .HasMaxLength(128)
                .HasColumnName("ftp_address");
            entity.Property(e => e.FtpLogin)
                .HasMaxLength(128)
                .HasDefaultValueSql("(N'anonymous')")
                .HasColumnName("ftp_login");
            entity.Property(e => e.FtpPassword)
                .HasMaxLength(524)
                .HasColumnName("ftp_password");
            entity.Property(e => e.FtpPort)
                .HasDefaultValueSql("((21))")
                .HasColumnName("ftp_port");
            entity.Property(e => e.FtpSubdirectory)
                .HasMaxLength(255)
                .HasColumnName("ftp_subdirectory");
            entity.Property(e => e.ImmediateSync).HasColumnName("immediate_sync");
            entity.Property(e => e.ImmediateSyncReady).HasColumnName("immediate_sync_ready");
            entity.Property(e => e.IndependentAgent).HasColumnName("independent_agent");
            entity.Property(e => e.MinAutonosyncLsn)
                .HasMaxLength(10)
                .IsFixedLength()
                .HasColumnName("min_autonosync_lsn");
            entity.Property(e => e.Name)
                .HasMaxLength(128)
                .HasColumnName("name");
            entity.Property(e => e.Options).HasColumnName("options");
            entity.Property(e => e.OriginatorId).HasColumnName("originator_id");
            entity.Property(e => e.PostSnapshotScript)
                .HasMaxLength(255)
                .HasColumnName("post_snapshot_script");
            entity.Property(e => e.PreSnapshotScript)
                .HasMaxLength(255)
                .HasColumnName("pre_snapshot_script");
            entity.Property(e => e.Pubid)
                .ValueGeneratedOnAdd()
                .HasColumnName("pubid");
            entity.Property(e => e.QueueType).HasColumnName("queue_type");
            entity.Property(e => e.ReplFreq).HasColumnName("repl_freq");
            entity.Property(e => e.ReplicateDdl)
                .HasDefaultValueSql("((1))")
                .HasColumnName("replicate_ddl");
            entity.Property(e => e.Retention).HasColumnName("retention");
            entity.Property(e => e.SnapshotInDefaultfolder)
                .IsRequired()
                .HasDefaultValueSql("((1))")
                .HasColumnName("snapshot_in_defaultfolder");
            entity.Property(e => e.SnapshotJobid)
                .HasMaxLength(16)
                .IsFixedLength()
                .HasColumnName("snapshot_jobid");
            entity.Property(e => e.Status).HasColumnName("status");
            entity.Property(e => e.SyncMethod).HasColumnName("sync_method");
        });

        modelBuilder.Entity<Sysreplserver>(entity =>
        {
            entity.HasKey(e => e.Srvname).HasName("PK__sysrepls__10A5BE6332983C28");

            entity.ToTable("sysreplservers");

            entity.Property(e => e.Srvname)
                .HasMaxLength(128)
                .HasColumnName("srvname");
            entity.Property(e => e.Srvid).HasColumnName("srvid");
        });

        modelBuilder.Entity<Sysschemaarticle>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("sysschemaarticles");

            entity.HasIndex(e => new { e.Artid, e.Pubid }, "c1sysschemaarticles")
                .IsUnique()
                .IsClustered();

            entity.Property(e => e.Artid).HasColumnName("artid");
            entity.Property(e => e.CreationScript)
                .HasMaxLength(255)
                .HasColumnName("creation_script");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
            entity.Property(e => e.DestObject)
                .HasMaxLength(128)
                .HasColumnName("dest_object");
            entity.Property(e => e.DestOwner)
                .HasMaxLength(128)
                .HasColumnName("dest_owner");
            entity.Property(e => e.Name)
                .HasMaxLength(128)
                .HasColumnName("name");
            entity.Property(e => e.Objid).HasColumnName("objid");
            entity.Property(e => e.PreCreationCmd).HasColumnName("pre_creation_cmd");
            entity.Property(e => e.Pubid).HasColumnName("pubid");
            entity.Property(e => e.SchemaOption)
                .HasMaxLength(8)
                .IsFixedLength()
                .HasColumnName("schema_option");
            entity.Property(e => e.Status).HasColumnName("status");
            entity.Property(e => e.Type).HasColumnName("type");
        });

        modelBuilder.Entity<Syssubscription>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("syssubscriptions");

            entity.HasIndex(e => new { e.Artid, e.Srvid, e.DestDb, e.Srvname }, "unc1syssubscriptions").IsUnique();

            entity.Property(e => e.Artid).HasColumnName("artid");
            entity.Property(e => e.DestDb)
                .HasMaxLength(128)
                .HasColumnName("dest_db");
            entity.Property(e => e.DistributionJobid)
                .HasMaxLength(16)
                .IsFixedLength()
                .HasColumnName("distribution_jobid");
            entity.Property(e => e.LoginName)
                .HasMaxLength(128)
                .HasColumnName("login_name");
            entity.Property(e => e.LoopbackDetection).HasColumnName("loopback_detection");
            entity.Property(e => e.NosyncType).HasColumnName("nosync_type");
            entity.Property(e => e.QueuedReinit).HasColumnName("queued_reinit");
            entity.Property(e => e.Srvid).HasColumnName("srvid");
            entity.Property(e => e.Srvname)
                .HasMaxLength(128)
                .HasDefaultValueSql("(N'')")
                .HasColumnName("srvname");
            entity.Property(e => e.Status).HasColumnName("status");
            entity.Property(e => e.SubscriptionType).HasColumnName("subscription_type");
            entity.Property(e => e.SyncType).HasColumnName("sync_type");
            entity.Property(e => e.Timestamp)
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("timestamp");
            entity.Property(e => e.UpdateMode).HasColumnName("update_mode");
        });

        modelBuilder.Entity<Systranschema>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("systranschemas");

            entity.HasIndex(e => e.Startlsn, "uncsystranschemas")
                .IsUnique()
                .IsClustered();

            entity.Property(e => e.Endlsn)
                .HasMaxLength(10)
                .IsFixedLength()
                .HasColumnName("endlsn");
            entity.Property(e => e.Startlsn)
                .HasMaxLength(10)
                .IsFixedLength()
                .HasColumnName("startlsn");
            entity.Property(e => e.Tabid).HasColumnName("tabid");
            entity.Property(e => e.Typeid)
                .HasDefaultValueSql("((52))")
                .HasColumnName("typeid");
        });

        modelBuilder.Entity<Token>(entity =>
        {
            entity.Property(e => e.TokenId).HasColumnName("TokenID");
            entity.Property(e => e.AccessToken)
                .HasMaxLength(5000)
                .IsUnicode(false);
            entity.Property(e => e.AtexpiryTime).HasColumnName("ATExpiryTime");
            entity.Property(e => e.Ipaddress)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasColumnName("IPAddress");
            entity.Property(e => e.LoginLocation)
                .HasMaxLength(1000)
                .IsUnicode(false);
            entity.Property(e => e.RefreshToken)
                .HasMaxLength(1000)
                .IsUnicode(false);
            entity.Property(e => e.RtexpiryTime).HasColumnName("RTExpiryTime");
            entity.Property(e => e.UserId).HasColumnName("UserID");
        });

        modelBuilder.Entity<Transaction>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Transact__3214EC0747DBAE45");

            entity.Property(e => e.AdditionalFee).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.Amount).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.ApprovalCode).HasMaxLength(50);
            entity.Property(e => e.CardHolder).HasMaxLength(150);
            entity.Property(e => e.ClientIp)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("ClientIP");
            entity.Property(e => e.Currency).HasMaxLength(5);
            entity.Property(e => e.CustomerEmail).HasMaxLength(50);
            entity.Property(e => e.CustomerName).HasMaxLength(50);
            entity.Property(e => e.Description).HasMaxLength(300);
            entity.Property(e => e.EndRecur).HasColumnType("text");
            entity.Property(e => e.Fee).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.Frequency).HasColumnType("text");
            entity.Property(e => e.IsRecurr).HasColumnName("isRecurr");
            entity.Property(e => e.Mpistatus)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("MPIStatus");
            entity.Property(e => e.OnCardReturnUrl).HasMaxLength(500);
            entity.Property(e => e.OrderType)
                .HasMaxLength(500)
                .IsFixedLength()
                .HasColumnName("orderType");
            entity.Property(e => e.Pan)
                .HasMaxLength(50)
                .HasColumnName("PAN");
            entity.Property(e => e.Parameter).HasMaxLength(4000);
            entity.Property(e => e.ProcessorRef).HasMaxLength(1500);
            entity.Property(e => e.ReferenceId)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("ReferenceID");
            entity.Property(e => e.RespCode).HasComputedColumnSql("(case when lower([Status])='initiated' then (0) when lower([Status]) like 'approved%' then (1) when lower([Status]) like 'cancelled%' then (2) else (3) end)", false);
            entity.Property(e => e.RetailerId).HasMaxLength(50);
            entity.Property(e => e.ReturnUrl).HasMaxLength(150);
            entity.Property(e => e.Scheme).HasMaxLength(50);
            entity.Property(e => e.SessionId).HasMaxLength(100);
            entity.Property(e => e.Status).HasMaxLength(350);
            entity.Property(e => e.StatusDescription).HasMaxLength(500);
            entity.Property(e => e.SubMerchantCity).HasMaxLength(150);
            entity.Property(e => e.SubMerchantCountryCode).HasMaxLength(150);
            entity.Property(e => e.SubMerchantId).HasMaxLength(150);
            entity.Property(e => e.SubMerchantName).HasMaxLength(150);
            entity.Property(e => e.SubMerchantPostalCode).HasMaxLength(150);
            entity.Property(e => e.SubMerchantStreetAddress).HasMaxLength(150);
            entity.Property(e => e.SwitchId)
                .HasMaxLength(50)
                .IsFixedLength();
            entity.Property(e => e.TerminalId).HasMaxLength(50);
            entity.Property(e => e.TripleDesIv)
                .HasMaxLength(50)
                .HasColumnName("TripleDesIV");
            entity.Property(e => e.VendorId).HasMaxLength(50);

            entity.HasOne(d => d.Merchant).WithMany(p => p.Transactions)
                .HasForeignKey(d => d.MerchantId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Transacti__Merch__59063A47");

            entity.HasOne(d => d.Route).WithMany(p => p.Transactions)
                .HasForeignKey(d => d.RouteId)
                .HasConstraintName("FK_Transactions_Routes");
        });

        modelBuilder.Entity<Upload>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_Uploads_PartyId");

            entity.HasIndex(e => e.UserId, "IX_Uploads_UserID");

            entity.Property(e => e.UploadId).HasColumnName("UploadID");
            entity.Property(e => e.UserId).HasColumnName("UserID");

            entity.HasOne(d => d.Party).WithMany(p => p.Uploads).HasForeignKey(d => d.PartyId);

            entity.HasOne(d => d.User).WithMany(p => p.Uploads).HasForeignKey(d => d.UserId);
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.Property(e => e.UserId).HasColumnName("UserID");
            entity.Property(e => e.EmailAddress)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Facebook).HasMaxLength(150);
            entity.Property(e => e.FirstName)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.Instagram).HasMaxLength(150);
            entity.Property(e => e.LastName)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.LinkedIn).HasMaxLength(150);
            entity.Property(e => e.Password)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PasswordSalt)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PhoneNumber)
                .HasMaxLength(30)
                .IsUnicode(false);
            entity.Property(e => e.Twitter).HasMaxLength(150);
            entity.Property(e => e.Website).HasMaxLength(150);
            entity.Property(e => e.Youtube).HasMaxLength(150);
        });

        modelBuilder.Entity<UserParty>(entity =>
        {
            entity.HasKey(e => new { e.UserId, e.PartyId });

            entity.HasIndex(e => e.PartyId, "IX_UserParties_PartyId");

            entity.Property(e => e.UserId).HasColumnName("UserID");

            entity.HasOne(d => d.Party).WithMany(p => p.UserParties).HasForeignKey(d => d.PartyId);

            entity.HasOne(d => d.User).WithMany(p => p.UserParties).HasForeignKey(d => d.UserId);
        });

        modelBuilder.Entity<UserRole>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_UserRoles_PartyId");

            entity.HasIndex(e => e.RoleId, "IX_UserRoles_RoleID");

            entity.HasIndex(e => e.UserId, "IX_UserRoles_UserID");

            entity.Property(e => e.UserRoleId).HasColumnName("UserRoleID");
            entity.Property(e => e.RoleId).HasColumnName("RoleID");
            entity.Property(e => e.UserId).HasColumnName("UserID");

            entity.HasOne(d => d.Party).WithMany(p => p.UserRoles).HasForeignKey(d => d.PartyId);

            entity.HasOne(d => d.Role).WithMany(p => p.UserRoles).HasForeignKey(d => d.RoleId);

            entity.HasOne(d => d.User).WithMany(p => p.UserRoles).HasForeignKey(d => d.UserId);
        });

        modelBuilder.Entity<WorkflowActivation>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_WorkflowActivations_PartyId");

            entity.HasIndex(e => e.WorkflowActivationStepId, "IX_WorkflowActivations_WorkflowActivationStepID");

            entity.Property(e => e.WorkflowActivationId).HasColumnName("WorkflowActivationID");
            entity.Property(e => e.UserId).HasColumnName("UserID");
            entity.Property(e => e.WorkflowActivationStepId).HasColumnName("WorkflowActivationStepID");

            entity.HasOne(d => d.Party).WithMany(p => p.WorkflowActivations).HasForeignKey(d => d.PartyId);

            entity.HasOne(d => d.WorkflowActivationStep).WithMany(p => p.WorkflowActivations).HasForeignKey(d => d.WorkflowActivationStepId);
        });

        modelBuilder.Entity<WorkflowActivationDetail>(entity =>
        {
            entity.HasIndex(e => e.UserId, "IX_WorkflowActivationDetails_UserID");

            entity.HasIndex(e => e.WorkflowActivationId, "IX_WorkflowActivationDetails_WorkflowActivationID");

            entity.Property(e => e.WorkflowActivationDetailId).HasColumnName("WorkflowActivationDetailID");
            entity.Property(e => e.Comment).IsUnicode(false);
            entity.Property(e => e.Reason).IsUnicode(false);
            entity.Property(e => e.UserId).HasColumnName("UserID");
            entity.Property(e => e.WorkflowActivationId).HasColumnName("WorkflowActivationID");
            entity.Property(e => e.WorkflowActivationStepId).HasColumnName("WorkflowActivationStepID");

            entity.HasOne(d => d.User).WithMany(p => p.WorkflowActivationDetails).HasForeignKey(d => d.UserId);

            entity.HasOne(d => d.WorkflowActivation).WithMany(p => p.WorkflowActivationDetails).HasForeignKey(d => d.WorkflowActivationId);
        });

        modelBuilder.Entity<WorkflowActivationLog>(entity =>
        {
            entity.HasIndex(e => e.PartyId, "IX_WorkflowActivationLogs_PartyId");

            entity.HasIndex(e => e.UserId, "IX_WorkflowActivationLogs_UserID");

            entity.HasIndex(e => e.WorkflowActivationStepId, "IX_WorkflowActivationLogs_WorkflowActivationStepID");

            entity.Property(e => e.WorkflowActivationLogId).HasColumnName("WorkflowActivationLogID");
            entity.Property(e => e.UserId).HasColumnName("UserID");
            entity.Property(e => e.WorkflowActivationStepId).HasColumnName("WorkflowActivationStepID");

            entity.HasOne(d => d.Party).WithMany(p => p.WorkflowActivationLogs).HasForeignKey(d => d.PartyId);

            entity.HasOne(d => d.User).WithMany(p => p.WorkflowActivationLogs).HasForeignKey(d => d.UserId);

            entity.HasOne(d => d.WorkflowActivationStep).WithMany(p => p.WorkflowActivationLogs).HasForeignKey(d => d.WorkflowActivationStepId);
        });

        modelBuilder.Entity<WorkflowActivationRequirement>(entity =>
        {
            entity.Property(e => e.WorkflowActivationRequirementId).HasColumnName("WorkflowActivationRequirementID");
            entity.Property(e => e.Description)
                .HasMaxLength(1000)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(30)
                .IsUnicode(false);
        });

        modelBuilder.Entity<WorkflowActivationStep>(entity =>
        {
            entity.HasIndex(e => e.RoleId, "IX_WorkflowActivationSteps_RoleID");

            entity.Property(e => e.WorkflowActivationStepId).HasColumnName("WorkflowActivationStepID");
            entity.Property(e => e.RoleId).HasColumnName("RoleID");

            entity.HasOne(d => d.Role).WithMany(p => p.WorkflowActivationSteps).HasForeignKey(d => d.RoleId);
        });

        modelBuilder.Entity<WorkflowActivationStepDetail>(entity =>
        {
            entity.HasIndex(e => e.WorkflowActivationRequirementId, "IX_WorkflowActivationStepDetails_WorkflowActivationRequirementID");

            entity.HasIndex(e => e.WorkflowActivationStepId, "IX_WorkflowActivationStepDetails_WorkflowActivationStepID");

            entity.Property(e => e.WorkflowActivationStepDetailId).HasColumnName("WorkflowActivationStepDetailID");
            entity.Property(e => e.WorkflowActivationRequirementId).HasColumnName("WorkflowActivationRequirementID");
            entity.Property(e => e.WorkflowActivationStepId).HasColumnName("WorkflowActivationStepID");

            entity.HasOne(d => d.WorkflowActivationRequirement).WithMany(p => p.WorkflowActivationStepDetails).HasForeignKey(d => d.WorkflowActivationRequirementId);

            entity.HasOne(d => d.WorkflowActivationStep).WithMany(p => p.WorkflowActivationStepDetails).HasForeignKey(d => d.WorkflowActivationStepId);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
