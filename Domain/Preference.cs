﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Preference
{
    public int PreferenceId { get; set; }

    public int PartyId { get; set; }

    public bool AllowCardPayment { get; set; }

    public bool AllowBankPayment { get; set; }

    public bool AllowUssdpayment { get; set; }

    public bool AllowQrpayment { get; set; }

    public bool SendEmailReceiptToCustomer { get; set; }

    public bool SendMeTransactionEmail { get; set; }

    public bool SendMeTransferEmail { get; set; }

    public bool SendRecipientTransferEmail { get; set; }

    public bool EnabledOtpconfirmation { get; set; }

    public bool SendOtptoEmail { get; set; }

    public bool TransactionFeeBearer { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual Party Party { get; set; } = null!;
}
