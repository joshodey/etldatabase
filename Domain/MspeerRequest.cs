﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MspeerRequest
{
    public int Id { get; set; }

    public string Publication { get; set; } = null!;

    public DateTime? SentDate { get; set; }

    public string? Description { get; set; }
}
