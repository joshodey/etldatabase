﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class SettlementItem
{
    public int TransactionId { get; set; }

    public int PartyId { get; set; }

    public decimal Amount { get; set; }

    public DateTime SettleDate { get; set; }

    public virtual Party Party { get; set; } = null!;

    public virtual Transaction Transaction { get; set; } = null!;
}
