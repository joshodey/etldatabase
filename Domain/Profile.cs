﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Profile
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public bool International { get; set; }

    public bool IsEnabled { get; set; }

    public virtual ICollection<Party> Parties { get; set; } = new List<Party>();

    public virtual ICollection<Route> Routes { get; set; } = new List<Route>();
}
