﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Sysreplserver
{
    public string Srvname { get; set; } = null!;

    public int? Srvid { get; set; }
}
