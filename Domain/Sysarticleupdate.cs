﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Sysarticleupdate
{
    public int Artid { get; set; }

    public int Pubid { get; set; }

    public int SyncInsProc { get; set; }

    public int SyncUpdProc { get; set; }

    public int SyncDelProc { get; set; }

    public bool Autogen { get; set; }

    public int SyncUpdTrig { get; set; }

    public int? ConflictTableid { get; set; }

    public int? InsConflictProc { get; set; }

    public bool IdentitySupport { get; set; }
}
