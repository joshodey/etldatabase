﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MspeerConflictdetectionconfigresponse
{
    public int RequestId { get; set; }

    public string PeerNode { get; set; } = null!;

    public string PeerDb { get; set; } = null!;

    public int? PeerVersion { get; set; }

    public int? PeerDbVersion { get; set; }

    public bool? IsPeer { get; set; }

    public bool? ConflictdetectionEnabled { get; set; }

    public int? OriginatorId { get; set; }

    public int? PeerConflictRetention { get; set; }

    public bool? PeerContinueOnconflict { get; set; }

    public string? PeerSubscriptions { get; set; }

    public string ProgressPhase { get; set; } = null!;

    public DateTime? ModifiedDate { get; set; }
}
