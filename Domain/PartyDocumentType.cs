﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class PartyDocumentType
{
    public int PartyDocumentTypeId { get; set; }

    public int PartyId { get; set; }

    public int DocumentTypeId { get; set; }

    public int UploadId { get; set; }
}
