﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class DisputeMessage
{
    public int DisputeMessageId { get; set; }

    public string Message { get; set; } = null!;

    public string ComposerType { get; set; } = null!;

    public string Email { get; set; } = null!;

    public int DisputeResolutionId { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual DisputeResolution DisputeResolution { get; set; } = null!;
}
