﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Transaction
{
    public int Id { get; set; }

    public int MerchantId { get; set; }

    public string Description { get; set; } = null!;

    public decimal Amount { get; set; }

    public decimal Fee { get; set; }

    public bool FeeInclusive { get; set; }

    public string Currency { get; set; } = null!;

    public DateTime Initiated { get; set; }

    public string? Status { get; set; }

    public string? ProcessorRef { get; set; }

    public DateTime? Completed { get; set; }

    public string ReturnUrl { get; set; } = null!;

    public string? Pan { get; set; }

    public string? CardHolder { get; set; }

    public int? RouteId { get; set; }

    public string? ApprovalCode { get; set; }

    public string? TripleDesIv { get; set; }

    public string? Parameter { get; set; }

    public string? OrderType { get; set; }

    public string? TerminalId { get; set; }

    public string? RetailerId { get; set; }

    public string? Scheme { get; set; }

    public int RespCode { get; set; }

    public string? SwitchId { get; set; }

    public string? VendorId { get; set; }

    public string? CustomerEmail { get; set; }

    public string? CustomerName { get; set; }

    public string? ClientIp { get; set; }

    public string? ReferenceId { get; set; }

    public string? Mpistatus { get; set; }

    public int? OrderExpirationPeriod { get; set; }

    public string? Frequency { get; set; }

    public string? EndRecur { get; set; }

    public bool? IsRecurr { get; set; }

    public decimal? AdditionalFee { get; set; }

    public string? SessionId { get; set; }

    public string? SubMerchantId { get; set; }

    public string? SubMerchantName { get; set; }

    public string? SubMerchantCity { get; set; }

    public string? SubMerchantCountryCode { get; set; }

    public string? SubMerchantPostalCode { get; set; }

    public string? SubMerchantStreetAddress { get; set; }

    public DateTime? RefundDate { get; set; }

    public string? OnCardReturnUrl { get; set; }

    public string? PayattitudeOnFileRef { get; set; }

    public string? CardOnFileRef { get; set; }

    public string? CardOnFileId { get; set; }

    public int? OrderTimeOut { get; set; }

    public int? CardId { get; set; }

    public string? StatusDescription { get; set; }

    public bool? DisablePayattitude { get; set; }

    public string? TextMess { get; set; }

    public string? MerchantRefData { get; set; }

    public virtual Party Merchant { get; set; } = null!;

    public virtual ICollection<MessageLog> MessageLogs { get; set; } = new List<MessageLog>();

    public virtual Route? Route { get; set; }

    public virtual ICollection<SettlementItem> SettlementItems { get; set; } = new List<SettlementItem>();
}
