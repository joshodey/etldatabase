﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MspeerTopologyresponse
{
    public int? RequestId { get; set; }

    public string Peer { get; set; } = null!;

    public int? PeerVersion { get; set; }

    public string PeerDb { get; set; } = null!;

    public int? OriginatorId { get; set; }

    public int? PeerConflictRetention { get; set; }

    public DateTime? ReceivedDate { get; set; }

    public string? ConnectionInfo { get; set; }
}
