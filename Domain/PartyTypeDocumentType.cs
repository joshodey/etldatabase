﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class PartyTypeDocumentType
{
    public int PartyTypeDocumentTypeId { get; set; }

    public int PartyTypeId { get; set; }

    public int DocumentTypeId { get; set; }

    public virtual DocumentType DocumentType { get; set; } = null!;

    public virtual PartyType PartyType { get; set; } = null!;
}
