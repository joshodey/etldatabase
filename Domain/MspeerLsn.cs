﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MspeerLsn
{
    public int Id { get; set; }

    public DateTime? LastUpdated { get; set; }

    public string Originator { get; set; } = null!;

    public string OriginatorDb { get; set; } = null!;

    public string OriginatorPublication { get; set; } = null!;

    public int? OriginatorPublicationId { get; set; }

    public int? OriginatorDbVersion { get; set; }

    public byte[]? OriginatorLsn { get; set; }

    public int? OriginatorVersion { get; set; }

    public int? OriginatorId { get; set; }
}
