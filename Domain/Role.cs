﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Role
{
    public int RoleId { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public string? RoleType { get; set; }

    public bool IsDefault { get; set; }

    public int PartyId { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual ICollection<RoleParty> RoleParties { get; set; } = new List<RoleParty>();

    public virtual ICollection<RolePermission> RolePermissions { get; set; } = new List<RolePermission>();

    public virtual ICollection<UserRole> UserRoles { get; set; } = new List<UserRole>();

    public virtual ICollection<WorkflowActivationStep> WorkflowActivationSteps { get; set; } = new List<WorkflowActivationStep>();
}
