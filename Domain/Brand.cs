﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Brand
{
    public int Id { get; set; }

    public string ShortName { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Logo { get; set; } = null!;

    public bool IsEnabled { get; set; }
}
