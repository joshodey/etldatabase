﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class PasswordHistory
{
    public int PasswordHistoryId { get; set; }

    public string EmailAddress { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string PasswordSalt { get; set; } = null!;

    public DateTime CreatedDate { get; set; }
}
