﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Notification
{
    public int NotificationId { get; set; }

    public int UserId { get; set; }

    public string SenderEmail { get; set; } = null!;

    public string SenderDisplayName { get; set; } = null!;

    public string Recipient { get; set; } = null!;

    public string Url { get; set; } = null!;

    public bool IsSent { get; set; }

    public bool IsEmail { get; set; }

    public int MessageId { get; set; }

    public string? DashboardMeassage { get; set; }

    public bool IsRead { get; set; }

    public string? OptionalParameters { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual MessageTemplate Message { get; set; } = null!;
}
