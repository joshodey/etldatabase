﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class UserParty
{
    public int UserId { get; set; }

    public int PartyId { get; set; }

    public bool IsActive { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual Party Party { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}
