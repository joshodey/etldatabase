﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class ErrorLog
{
    public int Id { get; set; }

    public string? CallPoint { get; set; }

    public string? ClassName { get; set; }

    public string? MethodName { get; set; }

    public string? Parameters { get; set; }

    public string Message { get; set; } = null!;

    public string Exception { get; set; } = null!;

    public DateTime? CreatedDate { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public int TransactionId { get; set; }

    public DateTime Timestamp { get; set; }

    public string? RequestAddress { get; set; }
}
