﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class IntegrationOption
{
    public int IntegrationOptionId { get; set; }

    public string Name { get; set; } = null!;

    public string Icon { get; set; } = null!;

    public string? Url { get; set; }

    public string? IntegrationType { get; set; }

    public bool IsActive { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }
}
