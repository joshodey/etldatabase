﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class AccountChangeRequest
{
    public int AccountChangeRequestId { get; set; }

    public int PartyId { get; set; }

    public string? PartyName { get; set; }

    public string? PartyDescription { get; set; }

    public int UploadId { get; set; }

    public int AccountId { get; set; }

    public int Status { get; set; }

    public string? Comment { get; set; }

    public DateTime CreatedDate { get; set; }

    public virtual BankAccount Account { get; set; } = null!;
}
