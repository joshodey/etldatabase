﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Card
{
    public int Id { get; set; }

    public string? Scheme { get; set; }

    public string? Data { get; set; }

    public string? UniqueId { get; set; }

    public DateTime? DateCreated { get; set; }

    public int? TransactionId { get; set; }

    public int? MerchantId { get; set; }
}
