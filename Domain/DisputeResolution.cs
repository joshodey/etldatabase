﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class DisputeResolution
{
    public int DisputeResolutionId { get; set; }

    public string ReferenceId { get; set; } = null!;

    public int PartyId { get; set; }

    public string? SenderEmail { get; set; }

    public string Title { get; set; } = null!;

    public string Message { get; set; } = null!;

    public string Status { get; set; } = null!;

    public string CategoryType { get; set; } = null!;

    public bool IsMerchant { get; set; }

    public int ResponderUserId { get; set; }

    public string? ResponderEmail { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual ICollection<DisputeMessage> DisputeMessages { get; set; } = new List<DisputeMessage>();
}
