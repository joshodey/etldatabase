﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class PaymentPlan
{
    public int PaymentPlanId { get; set; }

    public string PlanName { get; set; } = null!;

    public string PlanDescription { get; set; } = null!;

    public decimal PlanAmount { get; set; }

    public string? PaymentInterval { get; set; }

    public int PaymentFrequency { get; set; }

    public bool KeepWhenGoLive { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public int PartyId { get; set; }

    public virtual Party Party { get; set; } = null!;

    public virtual ICollection<PaymentPage> PaymentPages { get; set; } = new List<PaymentPage>();
}
