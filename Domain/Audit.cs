﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Audit
{
    public int AuditId { get; set; }

    public string TableName { get; set; } = null!;

    public string KeyValues { get; set; } = null!;

    public string? OldValues { get; set; }

    public string? NewValues { get; set; }

    public string CreatedBy { get; set; } = null!;

    public DateTime CreatedDate { get; set; }
}
