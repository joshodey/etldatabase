﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MerchantDetail
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? Code { get; set; }

    public string? SecretKey { get; set; }

    public int? MerchantId { get; set; }
}
