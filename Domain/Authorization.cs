﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Authorization
{
    public int Id { get; set; }

    public string Action { get; set; } = null!;

    public string? EntityId { get; set; }

    public string EntityType { get; set; } = null!;

    public string Requester { get; set; } = null!;

    public DateTime RequestTimestamp { get; set; }

    public bool? IsApproved { get; set; }

    public string? Authorizer { get; set; }

    public DateTime? AuthorizeTimestamp { get; set; }

    public string? EntityJson { get; set; }

    public int? MerchantId { get; set; }
}
