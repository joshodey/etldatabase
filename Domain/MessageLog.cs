﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MessageLog
{
    public int Id { get; set; }

    public int? TransactionId { get; set; }

    public string Service { get; set; } = null!;

    public string Direction { get; set; } = null!;

    public string RemoteEndpoint { get; set; } = null!;

    public string Message { get; set; } = null!;

    public DateTime Timestamp { get; set; }

    public virtual Transaction? Transaction { get; set; }
}
