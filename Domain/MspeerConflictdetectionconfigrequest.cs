﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MspeerConflictdetectionconfigrequest
{
    public int Id { get; set; }

    public string Publication { get; set; } = null!;

    public DateTime SentDate { get; set; }

    public int Timeout { get; set; }

    public DateTime ModifiedDate { get; set; }

    public string ProgressPhase { get; set; } = null!;

    public bool PhaseTimedOut { get; set; }
}
