﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Upload
{
    public int UploadId { get; set; }

    public int PartyId { get; set; }

    public int UserId { get; set; }

    public string FileModule { get; set; } = null!;

    public string FileName { get; set; } = null!;

    public string FileDescription { get; set; } = null!;

    public DateTime CreatedDate { get; set; }

    public virtual Party Party { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}
