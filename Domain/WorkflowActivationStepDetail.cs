﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class WorkflowActivationStepDetail
{
    public int WorkflowActivationStepDetailId { get; set; }

    public int WorkflowActivationStepId { get; set; }

    public int WorkflowActivationRequirementId { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual WorkflowActivationRequirement WorkflowActivationRequirement { get; set; } = null!;

    public virtual WorkflowActivationStep WorkflowActivationStep { get; set; } = null!;
}
