﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MspeerOriginatoridHistory
{
    public string OriginatorPublication { get; set; } = null!;

    public int OriginatorId { get; set; }

    public string OriginatorNode { get; set; } = null!;

    public string OriginatorDb { get; set; } = null!;

    public int OriginatorDbVersion { get; set; }

    public int OriginatorVersion { get; set; }

    public DateTime InsertedDate { get; set; }
}
