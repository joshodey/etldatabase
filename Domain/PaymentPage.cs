﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class PaymentPage
{
    public int PaymentPageId { get; set; }

    public string PageName { get; set; } = null!;

    public string PageDescription { get; set; } = null!;

    public string? Logo { get; set; }

    public bool IsFixedPaymentAmount { get; set; }

    public decimal Amount { get; set; }

    public bool ShouldCollectPhoneNumber { get; set; }

    public bool IsSubscription { get; set; }

    public string CustomLink { get; set; } = null!;

    public string? RedirectAfterPayment { get; set; }

    public string? SuccessMessage { get; set; }

    public string? SendNotificationToEmail { get; set; }

    public string? ExtraField { get; set; }

    public int PlanTypeId { get; set; }

    public int? PaymentPlanId { get; set; }

    public int PartyId { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual Party Party { get; set; } = null!;

    public virtual PaymentPlan? PaymentPlan { get; set; }
}
