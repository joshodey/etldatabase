﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class WorkflowActivationRequirement
{
    public int WorkflowActivationRequirementId { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual ICollection<WorkflowActivationStepDetail> WorkflowActivationStepDetails { get; set; } = new List<WorkflowActivationStepDetail>();
}
