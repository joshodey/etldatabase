﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class SettlementRule
{
    public int Id { get; set; }

    public int RouteId { get; set; }

    public int PartyId { get; set; }

    public decimal Value { get; set; }

    public string Type { get; set; } = null!;

    public virtual Party Party { get; set; } = null!;

    public virtual Route Route { get; set; } = null!;
}
