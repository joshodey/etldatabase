﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class RolePermission
{
    public int RolePermissionId { get; set; }

    public int RoleId { get; set; }

    public int PermissionId { get; set; }

    public int PartyId { get; set; }

    public virtual Party Party { get; set; } = null!;

    public virtual Permission Permission { get; set; } = null!;

    public virtual Role Role { get; set; } = null!;
}
