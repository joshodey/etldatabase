﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Prospect
{
    public int Id { get; set; }

    public string AccountName { get; set; } = null!;

    public string Bank { get; set; } = null!;

    public string AccountNumber { get; set; } = null!;

    public string AccountType { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Telephone { get; set; } = null!;

    public string Website { get; set; } = null!;

    public string Address { get; set; } = null!;

    public int Employees { get; set; }

    public DateTime? IncorporationDate { get; set; }

    public string? MerchantId { get; set; }

    public string? ProfileName { get; set; }

    public string Status { get; set; } = null!;

    public string Identity { get; set; } = null!;

    public bool IsApproved { get; set; }

    public DateTime? Registered { get; set; }

    public bool? FeeIncluded { get; set; }

    public string? Ipaddresses { get; set; }

    public string? Url { get; set; }

    public int? ProfileId { get; set; }

    public string? Code { get; set; }
    public string? MerId { get; set; }

    public string? TerminalId { get; set; }

    public string? RetailerId { get; set; }
}
