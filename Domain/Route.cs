﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Route
{
    public int Id { get; set; }

    public int ProfileId { get; set; }

    public string BrandName { get; set; } = null!;

    public string ProcessorCode { get; set; } = null!;

    public decimal FeeAmount { get; set; }

    public string FeeType { get; set; } = null!;

    public virtual Profile Profile { get; set; } = null!;

    public virtual ICollection<SettlementRule> SettlementRules { get; set; } = new List<SettlementRule>();

    public virtual ICollection<Transaction> Transactions { get; set; } = new List<Transaction>();
}
