﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Account
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string PasswordHash { get; set; } = null!;

    public string? Email { get; set; }

    public string? Phone { get; set; }

    public DateTime ActiveDate { get; set; }

    public DateTime SuspendDate { get; set; }

    public string? SuspendReason { get; set; }

    public DateTime LiquidationDate { get; set; }

    public string? LiquidationStory { get; set; }

    public decimal Balance { get; set; }

    public string? CurrentOtp { get; set; }

    public DateTime Otpexpires { get; set; }

    public int Otpstreak { get; set; }

    public bool CreditSms { get; set; }

    public bool CreditEmail { get; set; }

    public bool DebitSms { get; set; }

    public bool DebitEmail { get; set; }

    public decimal Lien { get; set; }
}
