﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class PaymentInvoice
{
    public string Customername { get; set; } = null!;

    public decimal Amount { get; set; }

    public string Invoicenumber { get; set; } = null!;

    public string? Customerinfo { get; set; }

    public string? Notetocustomer { get; set; }

    public string? Url { get; set; }

    public int? Transactionid { get; set; }

    public string? Email { get; set; }

    public string? AdditionalField1 { get; set; }

    public string? AdditionalField2 { get; set; }

    public int MerchantId { get; set; }

    public int Id { get; set; }

    public string? Status { get; set; }
}
