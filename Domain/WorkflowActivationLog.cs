﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class WorkflowActivationLog
{
    public int WorkflowActivationLogId { get; set; }

    public int PartyId { get; set; }

    public int WorkflowActivationStepId { get; set; }

    public int UserId { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual Party Party { get; set; } = null!;

    public virtual User User { get; set; } = null!;

    public virtual WorkflowActivationStep WorkflowActivationStep { get; set; } = null!;
}
