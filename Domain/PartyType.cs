﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class PartyType
{
    public int PartyTypeId { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public string IconName { get; set; } = null!;

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual ICollection<PartyTypeDocumentType> PartyTypeDocumentTypes { get; set; } = new List<PartyTypeDocumentType>();
}
