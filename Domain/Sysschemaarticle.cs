﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Sysschemaarticle
{
    public int Artid { get; set; }

    public string? CreationScript { get; set; }

    public string? Description { get; set; }

    public string DestObject { get; set; } = null!;

    public string Name { get; set; } = null!;

    public int Objid { get; set; }

    public int Pubid { get; set; }

    public byte PreCreationCmd { get; set; }

    public int Status { get; set; }

    public byte Type { get; set; }

    public byte[]? SchemaOption { get; set; }

    public string DestOwner { get; set; } = null!;
}
