﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class Syssubscription
{
    public int Artid { get; set; }

    public short Srvid { get; set; }

    public string DestDb { get; set; } = null!;

    public byte Status { get; set; }

    public byte SyncType { get; set; }

    public string LoginName { get; set; } = null!;

    public int SubscriptionType { get; set; }

    public byte[]? DistributionJobid { get; set; }

    public byte[] Timestamp { get; set; } = null!;

    public byte UpdateMode { get; set; }

    public bool LoopbackDetection { get; set; }

    public bool QueuedReinit { get; set; }

    public byte NosyncType { get; set; }

    public string Srvname { get; set; } = null!;
}
