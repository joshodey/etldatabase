﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class MspeerResponse
{
    public int? RequestId { get; set; }

    public string Peer { get; set; } = null!;

    public string PeerDb { get; set; } = null!;

    public DateTime? ReceivedDate { get; set; }
}
