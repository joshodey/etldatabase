﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class PasswordRecovery
{
    public int PasswordRecoveryId { get; set; }

    public string EmailAddress { get; set; } = null!;

    public string SecurityCode { get; set; } = null!;

    public DateTime ExpiryTime { get; set; }

    public DateTime CreatedDate { get; set; }
}
