﻿using System;
using System.Collections.Generic;

namespace EtlDataBase;

public partial class User
{
    public int UserId { get; set; }

    public string? LastName { get; set; }

    public string? FirstName { get; set; }

    public string? PhoneNumber { get; set; }

    public string EmailAddress { get; set; } = null!;

    public string? Password { get; set; }

    public string? PasswordSalt { get; set; }

    public bool IsDeveloper { get; set; }

    public bool IsConfirmed { get; set; }

    public bool IsDeactivated { get; set; }

    public string? ActivationCode { get; set; }

    public string? Website { get; set; }

    public string? Facebook { get; set; }

    public string? Twitter { get; set; }

    public string? LinkedIn { get; set; }

    public string? Instagram { get; set; }

    public string? Youtube { get; set; }

    public DateTime ActivationCodeExpiryDate { get; set; }

    public int UserType { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModifiedDate { get; set; }

    public virtual ICollection<RoleParty> RoleParties { get; set; } = new List<RoleParty>();

    public virtual ICollection<Upload> Uploads { get; set; } = new List<Upload>();

    public virtual ICollection<UserParty> UserParties { get; set; } = new List<UserParty>();

    public virtual ICollection<UserRole> UserRoles { get; set; } = new List<UserRole>();

    public virtual ICollection<WorkflowActivationDetail> WorkflowActivationDetails { get; set; } = new List<WorkflowActivationDetail>();

    public virtual ICollection<WorkflowActivationLog> WorkflowActivationLogs { get; set; } = new List<WorkflowActivationLog>();
}
